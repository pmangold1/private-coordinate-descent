import pandas as pd
import numpy as np

from sklearn import linear_model

# functions to compute objective values
def ls_obj(X, y, w):
    return 0.5 / X.shape[0] * np.linalg.norm(np.dot(X, w) - y) ** 2

def log_obj(X, y, w):
    res = y * np.dot(X, w)
    return np.log(1 + np.exp(- res)).mean();

def l1_obj(w, param):
    return param * np.linalg.norm(w, ord=1)

def l2_obj(w, param):
    return 0.5 * param * np.linalg.norm(w, ord=2) ** 2


def obj(X, y, w, loss, reg, reg_param):
    if loss == "ls":
        obj_loss = ls_obj(X, y, w)
    else:
        obj_loss = log_obj(X, y, w)

    if(reg == "l2"):
        reg_loss = l2_obj(w, reg_param)
    elif(reg == "l1"):
        reg_loss = l1_obj(w, reg_param)
    else:
        reg_loss = 0

    return obj_loss + reg_loss


# function that returns optimal coefficient and optimal value
def optimal_objective(X, y, loss, reg, reg_param, **kwargs):
    X = np.array(X, dtype="double")
    y = np.array(y, dtype="double")

    if loss in ["LeastSquares", "ls"]:
        if reg == "l2":
            model = linear_model.Ridge(alpha=X.shape[0] * reg_param, fit_intercept=False)
        elif reg == "l1":
            model = linear_model.Lasso(alpha=reg_param, fit_intercept=False, max_iter=10000)
        else:
            model = linear_model.LinearRegression(fit_intercept=False)

    if loss in ["Logistic", "log"]:
        if reg == "l2":
            C = 1.0 / (reg_param * X.shape[0])
            model = linear_model.LogisticRegression(C=C, solver="newton-cg", penalty="l2", fit_intercept=False, tol=1e-6, max_iter=np.inf)
        elif reg == "l1":
            C = 1.0 / (reg_param * X.shape[0])
            model = linear_model.LogisticRegression(C=C, solver="liblinear", penalty="l1", fit_intercept=False, tol=1e-6, max_iter=np.inf)
        else:
            model = linear_model.LogisticRegression(fit_intercept=False, solver="newton-cg", penalty="none", tol=1e-6, max_iter=np.inf)


    model.fit(X, y)
    coef_star = model.coef_.flatten()

    return coef_star, obj(X, y, coef_star, loss, reg, reg_param)

def get_optimal(dpath, loss, reg, reg_param=0, label="y"):

    dataset = pd.read_csv(dpath)
    X = dataset.loc[:, dataset.columns != label]
    y = dataset[label].copy()

    if(loss in ["Logistic", "log"]):
        y[y > 0] = 1
        y[y <= 0] = -1

    return optimal_objective(X, y, loss, reg, reg_param)
