import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd
import os
import time
import pickle
import multiprocessing

from optimals import optimal_objective
from pcoptim import LeastSquares, Logistic, L1Regularizer, L2Regularizer
from pcoptim import coordinate_descent, gradient_descent

from sklearn.model_selection import ParameterGrid
from sklearn.datasets import fetch_california_housing


palette = sns.color_palette("colorblind")

#--------------------------------------------------------------------------------
# technical stuff for saving results properly
#--------------------------------------------------------------------------------

dir_prefix = "results/"
dir_date = time.strftime("%d-%m-%y-%Hh%M")
save_dir = dir_prefix + dir_date + "/"
os.makedirs(save_dir, exist_ok=True)


#--------------------------------------------------------------------------------
# Datasets
#--------------------------------------------------------------------------------

def import_dataset(path):
    def load_csv(path):
        df = pd.read_csv(path)
        return np.array(df.loc[:,df.columns != "target"]), np.array(df["target"])

    X_raw, y_raw = load_csv(path + "_raw.csv")
    X_norm, y_norm = load_csv(path + "_norm.csv")

    return X_raw, y_raw, X_norm, y_norm


# load datasets from files
X_houses_raw, y_houses_raw, X_houses_norm, y_houses_norm = import_dataset("houses/data/houses")
X_california_raw, y_california_raw, X_california_norm, y_california_norm = import_dataset("california/data/california")
_, _, X_lasso, y_lasso = import_dataset("lasso/data/lasso")
X_electricity_raw, y_electricity_raw, X_electricity_norm, y_electricity_norm = import_dataset("electricity/data/electricity")


#--------------------------------------------------------------------------------
# Loss Functions
#--------------------------------------------------------------------------------

## create losses
# houses
l2reg_houses = L2Regularizer(1.0 / X_houses_raw.shape[0])
loss_houses_raw = Logistic(X_houses_raw, y_houses_raw, l2reg_houses)
loss_houses_norm = Logistic(X_houses_norm, y_houses_norm, l2reg_houses)

# electricity
l2reg_electricity = L2Regularizer(1.0 / X_electricity_raw.shape[0])
loss_electricity_raw = Logistic(X_electricity_raw, y_electricity_raw, l2reg_electricity)
loss_electricity_norm = Logistic(X_electricity_norm, y_electricity_norm, l2reg_electricity)

# california
l1reg_california_raw = L1Regularizer(1.0)
loss_california_raw = LeastSquares(X_california_raw, y_california_raw, l1reg_california_raw)

l1reg_california_norm = L1Regularizer(0.1)
loss_california_norm = LeastSquares(X_california_norm, y_california_norm, l1reg_california_norm)

# lasso
l1reg_lasso = L1Regularizer(30)
loss_lasso = LeastSquares(X_lasso, y_lasso, l1reg_lasso)




#--------------------------------------------------------------------------------
# optimals
#--------------------------------------------------------------------------------

opt = {
    "electricity_raw": optimal_objective(X_electricity_raw, y_electricity_raw, "log", "l2", 1.0 / X_electricity_raw.shape[0]),
    "electricity_norm": optimal_objective(X_electricity_norm, y_electricity_norm, "log", "l2", 1.0 / X_electricity_raw.shape[0]),
    "houses_raw": optimal_objective(X_houses_raw, y_houses_raw, "log", "l2", 1.0 / X_houses_raw.shape[0]),
    "houses_norm": optimal_objective(X_houses_norm, y_houses_norm, "log", "l2", 1.0 / X_houses_raw.shape[0]),
    "california_raw": optimal_objective(X_california_raw, y_california_raw, "ls", "l1", 1.0),
    "california_norm": optimal_objective(X_california_norm, y_california_norm, "ls", "l1", 0.1),
    "lasso": optimal_objective(X_lasso, y_lasso, "ls", "l1", 30),
}

with open(save_dir + "opt.pickle", "wb") as f:
    pickle.dump(opt, f)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# experiments
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# algos
algos = { "coordinate_descent": coordinate_descent,
          "gradient_descent": gradient_descent }

# losses
losses = {
    "electricity_raw": loss_electricity_raw,
    "electricity_norm": loss_electricity_norm,
    "houses_raw": loss_houses_raw,
    "houses_norm": loss_houses_norm,
    "california_raw": loss_california_raw,
    "california_norm": loss_california_norm,
    "lasso": loss_lasso
}

# function to execute algorithm (used to parallelize a bit)
def execute_algo(kwargs):
    global algos, losses
    algo = algos[kwargs["algo_name"]]
    loss = losses[kwargs["loss_name"]]

    w0 = np.zeros(loss.p_)
    return algo(loss, w0, delta=1.0/loss.n_**2, **(kwargs["params"])).obj_[-1]

# function to run everything
def run_tuning(algorithms, loss, epsilon, nb_runs=5):
    pool = multiprocessing.Pool(processes=nb_runs)
    ret = { algo.__name__: {} for algo, _ in algorithms }
    rng = np.random.default_rng(seed=42)


    for algo, parameter_sets in algorithms:
        print("~~~", algo.__name__)
        print(loss)
        grid = ParameterGrid(parameter_sets)

        for i, params in enumerate(grid):
            params["epsilon"] = epsilon
            map_params = [
                { "algo_name": algo.__name__,
                  "loss_name": loss,
                  "params": {
                      **params,
                      "seed": rng.integers(100000),
                      "nb_logs": 1
                  }
                 } for r in range(nb_runs)]

            ret[algo.__name__][frozenset(params.items())] = \
                pool.map(execute_algo, map_params)

            print("%d/%d\t%.3f\t%.3f\t\t%d\t" \
                  % (i, len(grid),
                     params["learning_rate"],
                     params["clip"],
                     params["max_iter"]),
                     ret[algo.__name__][frozenset(params.items())])

    return ret

#--------------------------------------------------------------------------------
# Parameter Grid
#--------------------------------------------------------------------------------

params_cd = {
    "max_iter": [2, 5, 10, 20, 50],
    "learning_rate": np.logspace(-2, 1, 10),
    "clip": np.logspace(-3, 6, 100)
}

params_sgd = {
    "batch_size": [1],
    "batch_strategy": ["random"],
    "max_iter": [2, 5, 10, 20, 50],
    "learning_rate": np.logspace(-6, 0, 10),
    "clip": np.logspace(-3, 6, 100)
}

#--------------------------------------------------------------------------------
# Run the algorithms
#--------------------------------------------------------------------------------

# electricity_raw
# ~~~~~~~~~~~~~~~

print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
print("               ELECTRICITY RAW                    ")
print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

ret = run_tuning(((coordinate_descent, params_cd),
                  (gradient_descent, params_sgd)),
                 "electricity_raw", 1)

with open(save_dir + "electricity_raw.pickle", "wb") as f:
   pickle.dump(ret, f)


# electricity_norm
# ~~~~~~~~~~~~~~~~

print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
print("               ELECTRICITY NORM                   ")
print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

ret = run_tuning(((coordinate_descent, params_cd),
                  (gradient_descent, params_sgd)),
                 "electricity_norm", 1)


with open(save_dir + "electricity_norm.pickle", "wb") as f:
    pickle.dump(ret, f)

# houses_raw
# ~~~~~~~~~~

print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
print("                 HOUSES RAW                       ")
print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

ret = run_tuning(((coordinate_descent, params_cd),
                  (gradient_descent, params_sgd)),
                 "houses_raw", 1)

with open(save_dir + "houses_raw.pickle", "wb") as f:
   pickle.dump(ret, f)


# houses_norm
# ~~~~~~~~~~~

print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
print("                 HOUSES NORM                      ")
print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

ret = run_tuning(((coordinate_descent, params_cd),
                  (gradient_descent, params_sgd)),
                 "houses_norm", 1)


with open(save_dir + "houses_norm.pickle", "wb") as f:
    pickle.dump(ret, f)


# california_raw
# ~~~~~~~~~~~~~~

print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
print("                 CALIFORNIA RAW                   ")
print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

ret = run_tuning(((coordinate_descent, params_cd),
                  (gradient_descent, params_sgd)),
                 "california_raw", 1)


with open(save_dir + "california_raw.pickle", "wb") as f:
    pickle.dump(ret, f)


# california_norm
# ~~~~~~~~~~~~~~~

print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
print("                 CALIFORNIA NORM                  ")
print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

ret = run_tuning(((coordinate_descent, params_cd),
                  (gradient_descent, params_sgd)),
                 "california_norm", 1)


with open(save_dir + "california_norm.pickle", "wb") as f:
    pickle.dump(ret, f)

# lasso
# ~~~~~

print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
print("                 LASSO                            ")
print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")


ret = run_tuning(((coordinate_descent, params_cd),
                  (gradient_descent, params_sgd)),
                 "lasso", 10)

with open(save_dir + "lasso.pickle", "wb") as f:
    pickle.dump(ret, f)
