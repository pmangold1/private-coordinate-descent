import pickle
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

palette = sns.color_palette("colorblind")

#--------------------------------------------------------------------------------
# utlility functions
#--------------------------------------------------------------------------------

# to filter results by hyperparameters
def filter_results(results, filters):
    ret = {}
    for r in results:
        if filters.items() < dict(r).items():
            ret[r] = results[r]

    return ret

def get_best_last(results, filters):

    best = np.inf

    for r in filter_results(results, filters):
        if np.mean(results[r]) < best:
            best = np.mean(results[r])
            vals = results[r]

    return np.array(vals)

def plot_best(results, ax, filters, opt, algo, **kwargs):
    filters = filters.copy()

    max_iters = np.sort(np.unique([dict(r)["max_iter"]
                                   for r in filter_results(results, filters)]))

    values = np.array([get_best_last(results,
                                     {**filters, "max_iter": max_iter})
                       for max_iter in max_iters])

    mean_values = (np.mean(values, axis=1) - opt) / opt

    print("&", algo, " & ", end="")
    for all_vals in values:
        mean = (np.mean(all_vals) - opt) / opt
        std = np.std((all_vals - opt) / opt)
        if mean == np.min(mean_values):
            print("$ %.4f \\pm %.0e $* \t & " % (mean, std), end="")
        else:
            print("$ %.4f \\pm %.0e $ \t & " % (mean, std), end="")
    print()


    ax.plot(max_iters, (np.mean(values, axis=1) - opt) / opt, **kwargs)


#--------------------------------------------------------------------------------
# plot the results
#--------------------------------------------------------------------------------

datasets = {
    "houses_raw": "best_results/",
    "houses_norm": "best_results/",
    "electricity_raw": "best_results/",
    "electricity_norm": "best_results/",
    "california_raw": "best_results/",
    "california_norm": "best_results/",
    "lasso": "best_results/"
}

opt_dir = "best_results/"
with open(opt_dir + "opt.pickle", "rb") as f:
    opts = pickle.load(f)

max_iters = [2, 5, 10, 20, 50]
print("& Passes on data &", end="")
for max_iter in max_iters:
    print(str(max_iter), " \t &", end="")
print("\n\\hline")

for dataset, results_dir in datasets.items():

    epsilon = 10 if (dataset == "lasso") else 1

    print("\\hline")
    print(dataset, end=" ")

    path = results_dir + dataset + ".pickle"

    with open(path, "rb") as f:
        results = pickle.load(f)



    fig, ax = plt.subplots(1, 1, figsize=(4,3))

    ax.set_title(dataset)

    plot_best(results["coordinate_descent"], ax,
              {"epsilon": epsilon },
              opts[dataset][1], "DP-CD",
              color=palette[0])

    plot_best(results["gradient_descent"], ax,
               {"epsilon": epsilon, "batch_size": 1 },
               opts[dataset][1], "DP-SGD",
              color=palette[1])

    ax.set_yscale("log")

    # plt.tight_layout()
    # plt.show()
