# Differentially Private Coordinate Descent for Composite Empirical Risk Minimization

This repository contains the code linked to the ICML2022 paper [Differentially Private Coordinate Descent for Composite Empirical Risk Minimization](https://icml.cc/Conferences/2022/Schedule?showEvent=18090).

## Running the code

To generate the datasets, run the `datasets.ipynb` notebook.

The code of the algorithms is in the `pcoptim` directory, which is a small library in which we implemented our private algorithms.

To generate the plots, first install the `pcoptim` library. To this end, go to the `pcoptim` directory and type:

	mkdir build
	cd build
	cmake ..
	sudo make install

Then, to run DP-CD and DP-SGD, run
	
	python experiments.py 

Finally, to run the experiments for DP-SCD, run

	cd dp-scd
	./experiments_electricity.sh
	cd results
	./preprocess.sh
	python format_results.py
	cd ../..

To generate the plots, run 
	
	python optimization_plots.py
	python optimization_plots_small.py

Run `python max_iter_plot.py` to generate the table from Appendix G.
