import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import json
import pickle
import seaborn as sns

from pcoptim import LeastSquares, Logistic, L1Regularizer, L2Regularizer
from pcoptim import coordinate_descent, gradient_descent

import matplotlib
from matplotlib.lines import Line2D

#--------------------------------------------------------------------------------
# plot parameters
#--------------------------------------------------------------------------------

matplotlib.rcParams['figure.figsize'] = (4, 3)
matplotlib.rcParams['lines.markersize'] = 18
matplotlib.rcParams['axes.labelsize'] = 24
matplotlib.rcParams['lines.markersize'] = 12
matplotlib.rcParams['lines.linewidth'] = 5
matplotlib.rcParams['legend.fontsize'] = 22
matplotlib.rcParams['xtick.major.size'] = 12
matplotlib.rcParams['ytick.major.size'] = 12
matplotlib.rcParams['xtick.labelsize'] = 22
matplotlib.rcParams['ytick.labelsize'] = 22
matplotlib.rcParams['lines.markeredgewidth'] = 6


palette = sns.color_palette("colorblind")
palette2 = sns.color_palette("bright")


#--------------------------------------------------------------------------------
# datasets
#--------------------------------------------------------------------------------

def load_csv(path):
    df = pd.read_csv(path)
    return np.array(df.loc[:,df.columns != "target"]), np.array(df["target"])

def import_dataset(path):
    X_raw, y_raw = load_csv(path + "_raw.csv")
    X_norm, y_norm = load_csv(path + "_norm.csv")

    return X_raw, y_raw, \
           X_norm, y_norm

def private_coord_smoothness(loss, epsilon):
    # twice too big maxs
    maxs = np.linalg.norm(loss.X_, axis=0, ord=np.inf) * 2
    lip = loss.get_all_lipschitz_coord()
    priv_lip = np.abs(lip + 1.0 / loss.n_ * np.random.laplace(scale=maxs * loss.p_ / epsilon))

    return priv_lip



#--------------------------------------------------------------------------------
# datasets
#--------------------------------------------------------------------------------

def import_dataset(path):
    def load_csv(path):
        df = pd.read_csv(path)
        return np.array(df.loc[:,df.columns != "target"]), np.array(df["target"])

    X_raw, y_raw = load_csv(path + "_raw.csv")
    X_norm, y_norm = load_csv(path + "_norm.csv")

    return X_raw, y_raw, X_norm, y_norm


# load datasets from files
X_houses_raw, y_houses_raw, X_houses_norm, y_houses_norm = import_dataset("houses/data/houses")
X_electricity_raw, y_electricity_raw, X_electricity_norm, y_electricity_norm = import_dataset("electricity/data/electricity")
X_california_raw, y_california_raw, X_california_norm, y_california_norm = import_dataset("california/data/california")
_, _, X_lasso, y_lasso = import_dataset("lasso/data/lasso")

## create losses
# houses
l2reg_houses = L2Regularizer(1.0 / X_houses_raw.shape[0])
loss_houses_raw = Logistic(X_houses_raw, y_houses_raw, l2reg_houses)
loss_houses_norm = Logistic(X_houses_norm, y_houses_norm, l2reg_houses)

# electricity
l2reg_electricity = L2Regularizer(1.0 / X_electricity_raw.shape[0])
loss_electricity_raw = Logistic(X_electricity_raw, y_electricity_raw, l2reg_electricity)
loss_electricity_norm = Logistic(X_electricity_norm, y_electricity_norm, l2reg_electricity)

# california
l1reg_california_raw = L1Regularizer(1.0)
loss_california_raw = LeastSquares(X_california_raw, y_california_raw, l1reg_california_raw)

l1reg_california_norm = L1Regularizer(0.1)
loss_california_norm = LeastSquares(X_california_norm, y_california_norm, l1reg_california_norm)

# lasso
l1reg_lasso = L1Regularizer(30)
loss_lasso = LeastSquares(X_lasso, y_lasso, l1reg_lasso)


#--------------------------------------------------------------------------------
# utlility functions
#--------------------------------------------------------------------------------

# to filter results by hyperparameters
def filter_results(results, filters):
    ret = {}
    for r in results:
        if filters.items() < dict(r).items():
            ret[r] = results[r]

    return ret

# get utility as function of clipping thresholds
# returns min, mean and max over all runs for each set of hyperparameters
def get_best_params(results, filters):

    best_val, best_params = np.inf, None

    for r in filter_results(results, filters):
        d = dict(r)

        curr = np.mean(results[r])

        if curr < best_val:
            best_val, best_params = curr, d

    # return
    print(dict(best_params))
    print("best:", best_val)
    return dict(best_params), best_val

# run algorithms
def run_algorithm(algo, loss, params, opt, coord_lip=None, seed=42, nb_logs=-1, nb_runs=10):

    rng = np.random.default_rng(seed=seed)

    results = np.zeros((nb_runs, nb_logs + 1))
    w0 = np.zeros(loss.p_)

    time = np.zeros(nb_logs + 1)

    print(algo.__name__, params)

    for r in range(nb_runs):

        seed = rng.integers(100000)
        curr = algo(loss, w0=w0, seed=seed, nb_logs=nb_logs, **params)
        results[r] = curr.obj_

        time = r/(r+1) * time + 1.0 / (r+1) * np.array(curr.elapsed_time_)

    x = curr.x_

    print(np.mean(results, axis=0))
    print()

    return x, time, \
           (np.min(results, axis=0) - opt) / opt, \
           (np.mean(results, axis=0) - opt) / opt, \
           (np.max(results, axis=0) - opt) / opt



# plot utility as a function of clipping thresholds
def plot(results, ax1, ax2, color, **kwargs):
    x, time, mins, means, maxs = results

    hdl1, = ax1.plot(x, means, color=color, **kwargs)
    hdl2, = ax2.plot(time, means, color=color, **kwargs)

    if "ls" in kwargs:
        ax1.fill_between(x, mins, maxs, alpha=0.3, color=color, ls=kwargs["ls"])
        ax2.fill_between(time, mins, maxs, alpha=0.3, color=color, ls=kwargs["ls"])
    else:
        ax1.fill_between(x, mins, maxs, alpha=0.3, color=color)
        ax2.fill_between(time, mins, maxs, alpha=0.3, color=color)

    return hdl1, hdl2

# run and plot
def run_and_plot(algo, loss, results, params, opt, ax1, ax2,
                 epsilon_lip=None, seed=42, nb_logs=-1, nb_runs=10,
                 color=palette[0], **kwargs):

    # get best params
    curr_params, vals = get_best_params(results[algo.__name__], params)

    # if private estimation of lipschitz constants is relevant, do it
    if epsilon_lip is not None:
        coord_lip = private_coord_smoothness(loss,  epsilon_lip * curr_params["epsilon"])
        curr_params["epsilon"] = (1 - epsilon_lip) * curr_params["epsilon"]

    # run algorithm with relevant parameters
    curr_results = run_algorithm(algo, loss, curr_params, opt,
                                 seed=seed, nb_runs=10, nb_logs=nb_logs)

    # plot on ax1 and ax2 (ax1=passes on data, ax2=time)
    return plot(curr_results, ax1, ax2,
                color=color, **kwargs)

def run_and_plot_all(loss, results_dir, name, params,
                     epsilon_lip=None, seed=42, nb_logs=(10,10),
                     nb_runs=10, **kwargs):

    rng=np.random.default_rng(seed=seed)

    with open(results_dir + name + ".pickle", "rb") as f:
        results = pickle.load(f)
    with open(results_dir + "opt.pickle", "rb") as f:
        opt = pickle.load(f)[name][1]

    print("opt :", opt)

    fig1, ax1 = plt.subplots()
    fig2, ax2 = plt.subplots()

    hdls1, hdls2 = [], []

    cd_hdls = run_and_plot(coordinate_descent, loss, results,
                           params, opt, ax1, ax2, nb_logs=nb_logs[0],
                           seed=rng.integers(100000),
                           nb_runs=nb_runs, color=palette[0],
                           marker="o")
    hdls1.append(cd_hdls[0])
    hdls2.append(cd_hdls[1])

    sgd_hdls = run_and_plot(gradient_descent, loss, results,
                            {**params, "batch_size": 1}, opt,
                            ax1, ax2, nb_logs=nb_logs[1],
                            seed=rng.integers(100000),
                            nb_runs=nb_runs, color=palette[1],
                            marker="^")
    hdls1.append(sgd_hdls[0])
    hdls2.append(sgd_hdls[1])

    if epsilon_lip is not None:
        cd2_hdls = run_and_plot(coordinate_descent, loss,
                                results, params, opt, ax1, ax2,
                                seed=rng.integers(100000),
                                epsilon_lip=epsilon_lip,
                                nb_logs=nb_logs[1], nb_runs=nb_runs,
                                color=palette[2], marker="+")

        hdls1.append(cd2_hdls[0])
        hdls2.append(cd2_hdls[1])

    ax1.set_xlabel("Passes on data")
    ax2.set_xlabel("Time (s)")

    return fig1, fig2, ax1, ax2, hdls1, hdls2



#--------------------------------------------------------------------------------
# plot the results
#--------------------------------------------------------------------------------

rng = np.random.default_rng(123)

# electricity raw
print("--------------------------------------------------------------------------------")
print("ELECTRICITY RAW")
print("--------------------------------------------------------------------------------")

fig1, fig2, ax1, ax2, hdls1, hdls2 = \
    run_and_plot_all(loss_electricity_raw,
                     "best_results/",
                     "electricity_raw",
                     { "epsilon": 1 },
                     seed=rng.integers(10000),
                     nb_logs=(10,10),
                     epsilon_lip=0.1)


with open("dp-scd/results_to_plot/result_dp_scd_elec_raw.pickle", "rb") as f:
    res_scd_elec_raw = pickle.load(f)
hdl1, hdl2 = plot(res_scd_elec_raw, ax1, ax2, color=palette[3], marker="+")
hdls1.append(hdl1)
hdls2.append(hdl2)

ax1.set_yscale("log")
ax1.set_xticks([0,10,20,30,40,50])
ax1.legend(hdls1, ["DP-CD", "DP-SGD", "DP-CD-P"])

ax2.set_yscale("log")
ax2.legend(hdls2, ["DP-CD", "DP-SGD", "DP-CD-P"])

fig1.savefig("lastplots/small_optimization_electricity_raw.pdf", bbox_inches="tight")
fig2.savefig("lastplots/small_time_optimization_electricity_raw.pdf", bbox_inches="tight")

# electricity norm
print("--------------------------------------------------------------------------------")
print("ELECTRICITY NORM")
print("--------------------------------------------------------------------------------")

fig1, fig2, ax1, ax2, hdls1, hdls2 = \
    run_and_plot_all(loss_electricity_norm,
                     "best_results/",
                     "electricity_norm",
                     { "epsilon": 1 },
                     nb_logs=(4,10),
                     seed=rng.integers(10000))

with open("dp-scd/results_to_plot/result_dp_scd_elec_norm.pickle", "rb") as f:
    res_scd_elec_raw = pickle.load(f)
hdl1, hdl2 = plot(res_scd_elec_raw, ax1, ax2, color=palette[3], marker="+")
hdls1.append(hdl1)
hdls2.append(hdl2)

ax1.set_yscale("log")
ax1.set_xticks([0,10,20,30,40,50])
# ax1.legend(hdls1, ["DP-CD", "DP-SGD", "DP-SCD"])

ax2.set_yscale("log")
# ax2.legend(hdls2, ["DP-CD", "DP-SGD", "DP-SCD"])

fig1.savefig("lastplots/small_optimization_electricity_norm.pdf", bbox_inches="tight")
fig2.savefig("lastplots/small_time_optimization_electricity_norm.pdf", bbox_inches="tight")


# california raw
print("--------------------------------------------------------------------------------")
print("CALIFORNIA RAW")
print("--------------------------------------------------------------------------------")

fig1, fig2, ax1, ax2, hdls1, hdls2 = \
    run_and_plot_all(loss_california_raw,
                     "best_results/",
                     "california_raw",
                     { "epsilon": 1 },
                     seed=rng.integers(10000),
                     nb_logs=(10,10),
                     epsilon_lip=0.1)
ax1.set_yscale("log")
ax1.set_xticks([0,10,20,30,40,50])

ax2.set_yscale("log")
fig1.savefig("lastplots/small_optimization_california_raw.pdf", bbox_inches="tight")
fig2.savefig("lastplots/small_time_optimization_california_raw.pdf", bbox_inches="tight")

# california norm

print("--------------------------------------------------------------------------------")
print("CALIFORNIA NORM")
print("--------------------------------------------------------------------------------")
fig1, fig2, ax1, ax2, hdls1, hdls2 = \
    run_and_plot_all(loss_california_norm,
                     "best_results/",
                     "california_norm",
                     { "epsilon": 1 },
                     nb_logs=(4,10),
                     seed=rng.integers(10000))
ax1.set_yscale("log")
ax1.set_xticks([0,10,20,30,40,50])


ax2.set_yscale("log")
fig1.savefig("lastplots/small_optimization_california_norm.pdf", bbox_inches="tight")
fig2.savefig("lastplots/small_time_optimization_california_norm.pdf", bbox_inches="tight")

# houses raw
print("--------------------------------------------------------------------------------")
print("HOUSES RAW")
print("--------------------------------------------------------------------------------")
fig1, fig2, ax1, ax2, hdls1, hdls2 = \
    run_and_plot_all(loss_houses_raw,
                     "best_results/",
                     "houses_raw",
                     { "epsilon": 1 },
                     seed=rng.integers(10000),
                     nb_logs=(10,10),
                     epsilon_lip=0.1)
ax1.set_yscale("log")
ax1.set_xticks([0,10,20,30,40,50])
ax1.set_ylim(0.01, None)

ax2.set_yscale("log")
fig1.savefig("lastplots/small_optimization_houses_raw.pdf", bbox_inches="tight")
fig2.savefig("lastplots/small_time_optimization_houses_raw.pdf", bbox_inches="tight")

# houses norm
print("--------------------------------------------------------------------------------")
print("HOUSES NORM")
print("--------------------------------------------------------------------------------")
fig1, fig2, ax1, ax2, hdls1, hdls2 = \
    run_and_plot_all(loss_houses_norm,
                     "best_results/",
                     "houses_norm",
                     { "epsilon": 1 },
                     nb_logs=(10,10),
                     seed=rng.integers(10000))
ax1.set_yscale("log")
ax1.set_xticks([0,10,20,30,40,50])

ax2.set_yscale("log")
fig1.savefig("lastplots/small_optimization_houses_norm.pdf", bbox_inches="tight")
fig2.savefig("lastplots/small_time_optimization_houses_norm.pdf", bbox_inches="tight")

# lasso
print("--------------------------------------------------------------------------------")
print("LASSO")
print("--------------------------------------------------------------------------------")
fig1, fig2, ax1, ax2, hdls1, hdls2 = \
    run_and_plot_all(loss_lasso,
                     "best_results/",
                     "lasso",
                     { "epsilon": 10 },
                     nb_logs=(6,10),
                     seed=rng.integers(10000))

ax1.set_xticks([0,1,2,3,4,5])
hdl = Line2D([0], [0], color=palette[3], marker="+")
hdls1 += [hdl]

ax1.legend(hdls1, ["DP-CD", "DP-SGD", "DP-SCD"], loc="upper right")


fig1.savefig("lastplots/small_optimization_lasso.pdf", bbox_inches="tight")
fig2.savefig("lastplots/small_time_optimization_lasso.pdf", bbox_inches="tight")
