import os
import pickle
import numpy as np
import pandas as pd

directory="preprocessed_results/elec_nor/"

vals=[0,5,10,15,20,25,30,35,40,45,50]


# normalized
time = 0
results = []
ref = 0.5101816652306245


for t, filename in enumerate(os.listdir(directory)):
    f = os.path.join(directory, filename)

    data = pd.read_csv(f, header=None)

    x = np.array(data[0])
    data_res = np.array(data[2])

    time = t/(t+1)*time + 1.0/(t+1)*np.array(data[6])
    results.append(data_res)

results = np.array(results)

tuple_results = (x[vals],
                 time[vals] / 1000,
                 ((np.min(results, axis=0) - ref) / ref)[vals],
                 ((np.mean(results, axis=0) - ref) / ref)[vals],
                 ((np.max(results, axis=0) - ref) / ref)[vals])
print(tuple_results)
with open("result_dp_scd_elec_norm.pickle", "wb") as f:
    pickle.dump(tuple_results, f)

directory="preprocessed_results/elec_raw/"



# normalized
time = 0
results = []
ref = 0.5568569454161826


for t, filename in enumerate(os.listdir(directory)):
    f = os.path.join(directory, filename)

    if len(data[6]) < 51:
        break

    data = pd.read_csv(f, header=None)
    x = np.array(data[0])
    data_res = np.array(data[2])

    if(len(np.array(data[6])) < 51):
        break
    time = t/(t+1)*time + 1.0/(t+1)*np.array(data[6])
    results.append(data_res)

results = np.array(results)


tuple_results = (x[vals],
                 time[vals] / 1000,
                 ((np.min(results, axis=0) - ref) / ref)[vals],
                 ((np.mean(results, axis=0) - ref) / ref)[vals],
                 ((np.max(results, axis=0) - ref) / ref)[vals])

with open("result_dp_scd_elec_raw.pickle", "wb") as f:
    pickle.dump(tuple_results, f)
