mkdir preprocessed_results
mkdir preprocessed_results/elec_raw
mkdir preprocessed_results/elec_nor

for i in elec_*; do
    cat $i | grep "eval:" | cut -c6- >> preprocessed_results/$i;
    cp preprocessed_results/$i preprocessed_results/${i:0:8}/$i
done

mv preprocessed_results/elec_nor preprocessed_results/elec_norm
