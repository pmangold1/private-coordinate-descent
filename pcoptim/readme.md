# pcoptim

## Description

pcoptim is a library for differentially private optimization.
As of now, it includes few algorithms and losses:

* algorithms : (stochastic) gradient descent, coordinate descent.
* loss : linear and logistic regression.
* regularization : l1, l2, and no regularization.

The library is still in development, and serves mostly research purposes.
It is available either as a C++ library, or as a Python package through bindigs.

## Dependences

pcoptim relies on
`Eigen` (version 3.4.0) for linear algebra,
`boost` (version 1.77) for some algorithmic structures,
`pybind11` (version 2.7.1) for Python bindings
and `catch2` (version 2.13.7) for unit testing.
It can be compiled using `cmake` (version 3.1).

To install relevant dependences

	sudo apt install libboost-dev catch libeigen3-dev cmake

Note however that most distributions might package older versions of these
packages, so you might need to install them (notably Eigen) manually.

## Compilation

Once the dependences listed above are installed, you can build the project.
The C++ library is header-only, and can be installed with the following,
starting from root directory

	mkdir cpp/build
	cd cpp/build
	cmake ..
	make
	sudo make install

To build and install it as a Python package, use the following

	mkdir build
	cd build
	cmake ..
	sudo make install

## Unit testing

The library is provided with extensive unit tests.
Those are bundled with the C++ library and can be run using CMake

	mkdir cpp/build
	cd cpp/build
	cmake ..
	make all test

Or, once compiled, simply by running the executable `cpp/build/tests/tests`.


## Examples

TODO
