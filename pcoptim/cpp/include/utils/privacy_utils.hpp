#pragma once
#include <cmath>
#include <vector>
#include <algorithm>
#include <iostream>
#include <limits>

#include <float.h>

const double golden_ratio = (std::sqrt(5) + 1.0) / 2.0;

// exp minus one for when x is small
double expm1(const double& x) {
  if(std::abs(x) < 1e-5) {
    return x + 0.5*x*x;
  }
  else {
    return std::exp(x) - 1;
  }
}


// golden section minimum search
template<typename Function>
double golden_section_search(Function func, double param_min=1, double param_max=-1,
			     const double& tol=1e-6) {

  // in this function, the function func is assumed to be decreasing
  double param_mean = 0;

  // if param_max is not high enough, find one that is high enough
  if(param_max < param_min) {
    param_max = (param_min + 1);

    while(func(param_max) > func(param_max + 1)) {

      //      std::cout << "pmax: " << param_max << " " << func(param_max) << " " << func(param_max + 1) << std::endl;
      param_max *= 2;
    }
  }

  // look for minimum
  double param_quantile1 = param_max - (param_max - param_min) / golden_ratio;
  double param_quantile2 = param_min + (param_max - param_min) / golden_ratio;

  while(param_max - param_min > tol) {

    //    std::cou << "gss: " << param_min << " " << param_max << std::endl;

    if(func(param_quantile1) < func(param_quantile2)) {
      param_max = param_quantile2;
    }
    else {
      param_min = param_quantile1;
    }

    param_quantile1 = param_max - (param_max - param_min) / golden_ratio;
    param_quantile2 = param_min + (param_max - param_min) / golden_ratio;

  }

  return 0.5 * (param_min + param_max);
}


// dichotomic research function
template<typename Function>
double dichotomic_arg_search(Function func, double desired_func_value,
			     double param_min=0, double param_max=-1,
			     const double& tol=1e-6, const bool& decreasing=true) {

  // in this function, the function func is assumed to be decreasing

  double factor = decreasing ? 1 : -1;
  double param_mean = 0;


  // if param_max is not high enough, find one that is high enough
  if(param_max < param_min || factor * func(param_max) > factor * desired_func_value) {
    param_max = (param_min + 1);

    while(factor * func(param_max) > factor * desired_func_value) {

      //      std::cout << "pmax: " << param_max << " " << func(param_max) << std::endl;

      param_max *= 2;
    }
  }

  // then search by dichotomy until desired precision is reached
  while(param_max - param_min > tol) {
    //    std::cout << "dicho: " << param_min << " " << param_max << std::endl;
    param_mean = 0.5 * (param_min + param_max);

    if(factor * func(param_mean) > factor * desired_func_value) {
      param_min = param_mean;
    }
    else {
      param_max = param_mean;
    }
  }

  return param_mean;
}



// epsilon for RDP
double epsilon_rdp(const int& k, const double& sigma, const double& delta) {
  return (float)k / (2.0 * std::pow(sigma, 2))
    + std::sqrt(2.0 * (float)k * std::log(1.0/delta)) / sigma;
}


// RDP of Gaussian Mechanism
double rdp_gaussian(const double& sigma, const double& alpha) {
  return alpha / (2.0 * sigma * sigma);
}

double log_add(const double& logx, const double& logy) {
  if(logx > logy) {
    return std::log1p(std::exp(logy - logx)) + logx;
  }
  else {
    return std::log1p(std::exp(logx - logy)) + logy;
  }
}

double log_sub(const double& logx, const double& logy) {
  return std::log(std::expm1(logx - logy)) + logy;
}

// adapted from scipy
// https://github.com/scipy/scipy/blob/master/scipy/special/cephes/ndtr.c
double ndtr(double a)
{
    double x, y, z;

    x = a / std::sqrt(2);
    z = fabs(x);

    if (z < 1.0/std::sqrt(2))
	y = 0.5 + 0.5 * erf(x);

    else {
	y = 0.5 * erfc(z);

	if (x > 0)
	    y = 1.0 - y;
    }

    return (y);
}

// adapted from scipy
// https://github.com/scipy/scipy/blob/master/scipy/special/cephes/ndtr.c
double log_ndtr(double a)
{

    double log_LHS,		/* we compute the left hand side of the approx (LHS) in one shot */
     last_total = 0,		/* variable used to check for convergence */
	right_hand_side = 1,	/* includes first term from the RHS summation */
	numerator = 1,		/* numerator for RHS summand */
	denom_factor = 1,	/* use reciprocal for denominator to avoid division */
	denom_cons = 1.0 / (a * a);	/* the precomputed division we use to adjust the denominator */
    long sign = 1, i = 0;

    if (a > 6) {
	return -ndtr(-a);     /* log(1+x) \approx x */
    }
    if (a > -20) {
	return log(ndtr(a));
    }
    log_LHS = -0.5 * a * a - log(-a) - 0.5 * log(2 * M_PI);

    while (fabs(last_total - right_hand_side) > DBL_EPSILON) {
	i += 1;
	last_total = right_hand_side;
	sign = -sign;
	denom_factor *= denom_cons;
	numerator *= 2 * i - 1;
	right_hand_side += sign * numerator * denom_factor;

    }
    return log_LHS + log(right_hand_side);
}


// adapted from Opacus
// https://github.com/pytorch/opacus/blob/main/opacus/privacy_analysis.py
// (also used in TF privacy
// https://github.com/tensorflow/privacy/blob/master/tensorflow_privacy/privacy/analysis/rdp_accountant.py )
double log_erfc(const double& x) {
  return std::log(2) + log_ndtr(-x * std::sqrt(2));
}


double binomial(const double& alpha, const int& k) {
  double ret = 1;

  for(int j = 1; j <= k; j++) {
    ret *= (alpha - j + 1) / j;
  }

  return ret;
}

double logabsbinomial(const double& alpha, const int& k) {
  double ret = 0;

  for(int j = 1; j <= k; j++) {
    ret += std::log(std::abs((alpha - j + 1) / j));
  }

  return ret;
}

double signbinomial(const double& alpha, const int& k) {
  double ret = 1;

  for(int j = 1; j <= k; j++) {
    if(alpha - j + 1 < 0)
      ret *= -1;
  }

  return ret;
}


// adapted from Opacus
// https://github.com/pytorch/opacus/blob/main/opacus/privacy_analysis.py
// resulting from the paper
// https://arxiv.org/pdf/1908.10530.pdf
double epsilon_sub_rdp_alpha(const int& k, const double& sigma,
			     const double& delta, const double& q,
			     const double& alpha) {
  double log_q = std::log(q);
  double log_1mq = std::log(1.0 - q);
  double sig2 = sigma * sigma;

  double log_a0 = -std::numeric_limits<double>::infinity();
  double log_a1 = -std::numeric_limits<double>::infinity();
  double log_t0, log_t1;
  double log_e0, log_e1;
  double log_s0 = 0, log_s1 = 0;
  int j = 0;

  double log_binom = 0;

  double z0 = sigma * sigma * std::log(1.0 / q - 1) + 0.5;

  double binom;
  double log_abs_binom;

  //  std::cout << k << " " << sigma << " " << delta << " " << q << " " << alpha << std::endl;

  while(log_s0 > -30 || log_s1 > -30) {

    //    if(j < 100)
      //      std::cout << log_s0 << " " << log_s1 << std::endl;

    //    binom = binomial(alpha, j); // legacy
    log_binom = logabsbinomial(alpha, j);

    log_t0 = log_binom + j * log_q + (alpha - j) * log_1mq;
    log_t1 = log_binom + (alpha - j) * log_q + j * log_1mq;

    log_e0 = std::log(0.5) + log_erfc((j - z0) / (std::sqrt(2) * sigma));
    log_e1 = std::log(0.5) + log_erfc((z0 - (alpha-j)) / (std::sqrt(2) * sigma));

    log_s0 = log_t0 + (j * j - j) / (2 * sig2) + log_e0;
    log_s1 = log_t1 + ((alpha-j)*(alpha-j) - (alpha-j)) / (2 * sig2) + log_e1;

    if(signbinomial(alpha, j) > 0) {
      log_a0 = log_add(log_a0, log_s0);
      log_a1 = log_add(log_a1, log_s1);
    }
    else {
      log_a0 = log_sub(log_a0, log_s0);
      log_a1 = log_sub(log_a1, log_s1);
    }

    j++;
  }

  return 1.0 / (alpha - 1.0) * (k * log_add(log_a0, log_a1) + std::log(1.0/delta));
}




// // epsilon for RDP with subsampling with fixed alpha
// double epsilon_sub_rdp_alpha(const int& k, const double& sigma,
// 		       const double& delta, const double& subsampling_ratio,
// 		       const double& alpha) {

//   int a = std::floor(alpha);
//   double log_sratio = std::log(subsampling_ratio);

//   double x;
//   double log_j_in_alpha = std::log(alpha) + std::log(alpha - 1) - std::log(2);
//   double sig2 = std::pow(sigma, 2);

//   double second_term_right = sig2 > 1.0 / std::log(2) ?
//     std::log(std::expm1(1.0/sig2)) + std::log(4)
//     : 1.0 / sig2 + std::log(2);
//   double max_x = 2 * log_sratio + log_j_in_alpha + second_term_right;

//   // first look for maximal value that will be used in exp
//   // we do not store the values since this can get quite big...
//   for(int j = 3; j < a+1; j++) {
//     log_j_in_alpha += std::log(alpha - j + 1.0) - std::log(j);
//     x = j * log_sratio + log_j_in_alpha + std::log(2) + 0.5 * j / sigma * (j-1) / sigma;
//     if(x > max_x) max_x = x;

//   }
//   if(max_x < 0) max_x = 0;

//   log_j_in_alpha = std::log(alpha) + std::log(alpha - 1) - std::log(2);
//   double sum_x = std::exp(-max_x)
//     + std::exp(2 * log_sratio + log_j_in_alpha + second_term_right - max_x);

//   // then compute the stable log (remove max value from exp and add
//   // it at the end.
//   for(int j = 3; j < a+1; j++) {
//     log_j_in_alpha += std::log(alpha - j + 1.0) - std::log(j);
//     x = j * log_sratio + log_j_in_alpha + std::log(2) + 0.5 * j / sigma * (j-1) / sigma;
//     sum_x += std::exp(x - max_x);

//   }

//   return 1.0 / (alpha - 1.0) * (k * (max_x + std::log(sum_x)) + std::log(1.0/delta));

// }

// epsilon for RDP with subsampling (function that guesses alpha)
double epsilon_sub_rdp(const int& k, const double& sigma,
		       const double& delta, const double& subsampling_ratio) {

  auto alpha_func = [&k, &delta, &subsampling_ratio, &sigma](const double& alpha) {
    return epsilon_sub_rdp_alpha(k, sigma, delta, subsampling_ratio, alpha);
  };

  double best_alpha = golden_section_search(alpha_func, 1);
  return epsilon_sub_rdp_alpha(k, sigma, delta, subsampling_ratio, best_alpha);
}

// return the best privacy constant
double best_privacy_constant_rdp(const int& k, const double& epsilon,
				 const double& delta,
				 const double& subsampling_ratio=-1) {

  auto epsilon_func = [&k, &epsilon, &delta, &subsampling_ratio](const double& sigma) {
    if(subsampling_ratio < 0) {
      return epsilon_rdp(k, sigma, delta);
    }
    else {
      return epsilon_sub_rdp(k, sigma, delta, subsampling_ratio);
    }
  };

  double t = dichotomic_arg_search(epsilon_func, epsilon, 0);
  return t;
}
