#pragma once
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>

using std::vector;
using std::string;
using std::cout;
using std::endl;
using std::ofstream;



/// Returns `true` if input is nan or infinity and `false otherwise`.
bool is_nan_or_inf(const double& x) {
    switch(std::fpclassify(x)) {
        case FP_INFINITE:  return true;
        case FP_NAN:       return true;
    }

    return false;
}

/// Generate a uniform repartition of points in a given interval.
/// First and last points are always kept.
/// @param max_iter Right-end of the interval [0, max_iter].
/// @param nb_logs Number of points to take in the sample.
vector<int> get_log_times(const int& max_iter, const int& nb_logs) {
  vector<int> times(nb_logs + 1);

  for(int t = 0; t < nb_logs; t++) {
    times[t] = std::ceil(t * max_iter / nb_logs);
  }
  times[nb_logs] = max_iter;

  return times;
}

class Log {
public:
  /// Final value of the coefficients.
  VectorXd final_coef;

  /// Finale value of the objective function.
  double final_obj;


  /// Default constructor.
  Log() {

  }

  /// Constructor.
  /// @param max_iter Nuber of iterations.
  /// @param nb_logs Number of logs to keep.
  Log(const int& max_iter, const int& nb_logs) {
    initialize(max_iter, nb_logs);
  }

  /// Constructor.
  /// @param times Indices to log at.
  /// @param x Number (float) of the epochs (not necessarily integer).
  /// @param obj Objective values.
  Log(const vector<int>& times,
      const vector<double>& x,
      const vector<double>& obj)
    : times_(times),
      x_(x),
      obj_(obj) {

  }

  /// Initialize the Log to a null value.
  /// @param max_iter Nuber of iterations.
  /// @param nb_logs Number of logs to keep.
  void initialize(const int& max_iter,
		  int nb_logs) {

    next_ = 0;

    // ensure no more logs than iterations
    if(max_iter <= nb_logs) {
      nb_logs = max_iter;
    }

    // initialize vectors
    times_ = get_log_times(max_iter, nb_logs);
    x_.resize(nb_logs+1);
    obj_.resize(nb_logs+1);
    elapsed_time_.resize(nb_logs+1);
  }

  /// Write objective value at current point.
  /// @param t Number of the epoch.
  /// @param obj Objective value.
  void write(const double& t,
	     const double& obj) {

    // write values to vectors
    x_[next_] = t;
    obj_[next_] = obj;

    elapsed_time_[next_] = (double)(clock() - start_time_) / CLOCKS_PER_SEC;

    // increase counter
    next_++;
  }

  /// Return `true` if `time` is the time to log and `false` otherwise.
  bool should_log(const int& time) const {
    // if time is bigger than next logging time, then we should log!
    return (time >= times_[next_]);
  }

  /// Print value of the stored objectives.
  void print_obj() const {
    cout << "Objective : ";
    for(unsigned int i = 0; i < obj_.size(); i++) {
      cout << std::fixed << std::setprecision(15) << obj_[i] << " ";
    }
    cout << endl;
  }

  /// Write logs to file.
  /// @param fpath Path of the file to save at.
  void write_to_file(const string& fpath) const {
    ofstream f;
    f.open(fpath);

    write_to_file(f);

    f.close();
  }

  /// Write logs to file.
  /// @param f Stream pointing to a file or other.
  void write_to_file(ofstream& f) const {
    f << "{\"obj\": [";
    for(unsigned int i = 0; i < obj_.size(); i++) {
      if(is_nan_or_inf(obj_[i])) {
	f << "Infinity";
      }
      else {
	f << std::fixed << std::setprecision(15) << obj_[i];
      }
      if(i < obj_.size() - 1) f << ", ";
    }
    f << "],\n \"x\": [";
    for(unsigned int i = 0; i < x_.size(); i++) {
      f << std::fixed << std::setprecision(15) << x_[i];
      if(i < x_.size() - 1) f << ", ";
    }
    f << "]}";
  }

  /// Initializer timer.
  void init_timer() {
    start_time_ = clock();
  }

  /// Return history of epochs.
  const vector<double> x() const { return x_; }

  /// Return history of objectives.
  const vector<double> obj() const { return obj_; }


  /// Return history of elapsed time.
  const vector<double> elapsed_time() const { return elapsed_time_; }


private:
  /// Times to log at.
  vector<int> times_;

  /// History of epochs.
  vector<double> x_;

  /// History of objectives.
  vector<double> obj_;

  /// Next time log is required.
  int next_;

  /// Starting time of clock.
  clock_t start_time_;

  /// Elapsed time history
  vector<double> elapsed_time_;
};
