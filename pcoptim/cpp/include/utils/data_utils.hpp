#include <Eigen/Dense>
#include <vector>
#include <fstream>

#include <boost/none_t.hpp>
#include <boost/optional.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/predicate.hpp>

using namespace Eigen;
using std::string;
using std::vector;
using std::stringstream;
using boost::lexical_cast;

/// Read data from a CSV file.
/// @param M Return type (assumed to be some kind of matrix).
/// @param fpath Path to file.
/// @param header A vector that is populated with headers of the file. If none,
/// ignore it. Note that if the file contains no header and this argument is
/// provided, the first line will be considered a header.
/// @return The data as a Matrix of type `M`.
template<typename M>
M read_csv (const std::string& fpath,
	    boost::optional< vector<string>& > header = boost::none) {
    std::ifstream indata;
    indata.open(fpath);


    string line;
    vector<string> head;
    vector<double> values;

    if(header != boost::none) {
      std::getline(indata, line);
      stringstream lineStream(line);
      string cell;
      while (std::getline(lineStream, cell, ',')) {
	(*header).push_back(cell);
      }
    }

    uint rows = 0;
    while (std::getline(indata, line)) {
        stringstream lineStream(line);
        string cell;
        while (std::getline(lineStream, cell, ',')) {
            values.push_back(std::stod(cell));
        }
        ++rows;
    }
    return Map<const Matrix<typename M::Scalar, M::RowsAtCompileTime, M::ColsAtCompileTime, RowMajor>>(values.data(), rows, values.size()/rows);
}

/// Remove a column of a matrix using its index. (Input is not modified.)
/// @param X The matrix.
/// @param col The index of the column to remove.
/// @return The matrix with the appropriate column removed.
MatrixXd drop_column(const MatrixXd& X, const unsigned int& col) {
  MatrixXd ret(X.rows(), X.cols() - 1);

  unsigned int t = 0;
  for(unsigned int i = 0; i < X.cols(); i++) {
    if(i != col) {
      ret.col(t) = X.col(i);
      t++;
    }
  }

  return ret;
}


/// Remove a column of a matrix using its name. (Input is not modified.)
/// @param X The matrix.
/// @param header Headers of the matrix.
/// @param col The name of the column to remove.
/// @return The matrix with the appropriate column removed.
MatrixXd drop_column(const MatrixXd& X, const vector<string>& header, const string& col) {
  try {
    auto it = std::find(header.begin(), header.end(), col);



    if(it == header.end()) {
      throw -1;
    }

    int index = it - header.begin();

    return drop_column(X, index);
  }
  catch (int e) {
    std::cout << "drop_column: column name not found in header." << std::endl;
    exit(-1);
  }
}

/// Return one column of a matrix from its name.
/// @param data The complete matrix.
/// @param header Headers of the matrix.
/// @param target Name of the column to return.
/// @return Column `target` of the matrix `data`.
VectorXd get_target(const MatrixXd& data, const vector<string>& header, const string& target) {
  try {
    auto it = std::find(header.begin(), header.end(), target);

    if(it == header.end()) {
      throw -1;
    }

    int index = it - header.begin();
    return data.col(index);
  }
  catch (int e) {
    std::cout << "get_target: target \"" << target << "\" not found in header." << std::endl;
    exit(-1);
  }
}

/// (Obsolete) Format file name from arguments.
string format_file_name(const string& dir,
			const string& algo,
			const string& loss,
			const string& reg,
			const double& reg_param,
			const int& max_iter,
			const double& learning_rate,
			const double& epsilon,
			const double& delta,
			const int& mec,
			const int& batch_size,
			const string& batch_strategy,
			const string& coord_strategy,
			const double& alpha_sample,
			const double& clip,
			const double& alpha_clip,
			const int & nb_runs) {

  string out = dir;

  if(!boost::algorithm::ends_with(out, "/"))
    out += "/";

  out += "algo";
  out += algo;
  out += "_loss";
  out += loss;
  out += "_reg";
  out += reg;
  out += "_";
  out += lexical_cast<string>(reg_param);
  out += "_T";
  out += lexical_cast<string>(max_iter);
  out += "_lr";
  out += lexical_cast<string>(learning_rate);
  out += "_eps";
  out += lexical_cast<string>(epsilon);
  out += "_delta";
  out += lexical_cast<string>(delta);
  out += "_mec";
  out += lexical_cast<string>(mec);
  out += "_batch";
  out += lexical_cast<string>(batch_size);
  out += "_";
  out += batch_strategy;
  out += "_coord";
  out += coord_strategy;
  out += lexical_cast<string>(alpha_sample);
  out += "_clip";
  out += lexical_cast<string>(clip);
  out += "_";
  out += lexical_cast<string>(alpha_clip);;
  out += "_nruns";
  out += lexical_cast<string>(nb_runs);
  out += ".result";

  return out;
}
