#ifndef LOSS_HEADER_
#define LOSS_HEADER_

#include <Eigen/Dense>

#include <boost/none_t.hpp>
#include <boost/optional.hpp>
#include <boost/range/adaptor/indexed.hpp>
#include <optional>
#include <string>
#include <memory>
#include <cmath>

#include "regularizer.hpp"

using Eigen::MatrixXd;
using Eigen::VectorXcd;
using Eigen::VectorXi;
using Eigen::JacobiSVD;

using boost::optional;
using boost::none;

double clip(const double& v, const optional<double>& c) {
  if(c == none) {
    return v;
  }
  else {
    if(std::abs(v) < *c) {
      return v;
    }
    else if(v > 0) {
      return *c;
    }
    else {
      return -*c;
    }
  }
}

VectorXd clip(const VectorXd& v, const optional<double>& c) {

  if(c == none) {
    return v;
  }
  else {
    /// TODO: implement clipping with arbitrary p-norm.
    double norm = v.template lpNorm<2>();
    if(norm < *c) {
      return v;
    }
    else {
      return v / norm * (*c);
    }
  }
}

class Loss {
public:

  // CONTSRUCTORS AND DESTRUCTOR

  /// Default constructor.
  Loss()
    : regularizer_(new NullRegularizer()),
      initialized_regularizer_(true) {

  }


  /// Constructor from design matrix and labels. No regularization.
  /// @param X The design matrix.
  /// @param y The labels.
  Loss(const MatrixXd& X, const VectorXd& y)
    : X_(X), y_(y), n_(X.rows()), p_(X.cols()),
      regularizer_(new NullRegularizer()),
      initialized_regularizer_(true) {

  }

  /// Constructor from design matrix and labels. Uses regularizer as
  /// regularization.
  /// @param X The design matrix.
  /// @param y The labels.
  /// @param regularizer The regularizer.
  Loss(const MatrixXd& X, const VectorXd& y, const Regularizer* regularizer)
    : X_(X), y_(y), n_(X.rows()), p_(X.cols()),
      regularizer_(regularizer),
      initialized_regularizer_(false) {
  }

  /// Destructor.
  virtual ~Loss() {
    if(initialized_regularizer_) {
      delete regularizer_;
    }
  }


  // PUBLIC FUNCTIONS LINKING INTERNAL PRIVATE FUNCTIONS WITH OUTSIDE

  // sensitivity
  /// Compute the sensitivity of the Loss.
  virtual double sensitivity(const VectorXd&) const = 0;

  /// Compute the sensitivity of the Loss along a given parameter.
  virtual double coord_sensitivity(const VectorXd&, const int&) const = 0;


  // lipschitz
  /// Compute the gradient lipschitz constant of the (regularized) Loss.
  double lipschitz() const {
    double lip = lipschitz_() + regularizer_->lipschitz();

    return lip;
  }

  /// Compute the coordinate gradient lipschitz constant of the
  /// (regularized) Loss.
  double coord_lipschitz(const int& j) const {
    double lip = coord_lipschitz_(j) + regularizer_->coord_lipschitz(j);

    return lip;
  }

  /// Compute all coordinate gradient lipschitz constants of the (regularized)
  /// Loss.
  VectorXd get_all_lipschitz_coord() const {
    VectorXd all(p_);
    for(unsigned int j = 0; j < p_; j++) {
      all(j) = coord_lipschitz(j);
    }

    return all;
  }


  // residuals
  /// Compute the residuals of the Loss.
  /// @param w Parameters to compute the residuals at.
  /// @param batch Indices of records to used for computing the residuals.
  /// If none (default), use all records.
  /// @param residuals Pre-computed residuals. If not none, just return this
  /// vector. (This is useful when residuals are updated dynamically in some
  /// optimization algorithms.)
  /// @return Vector of residuals if `residuals` is passed. Otherwise `residuals`
  /// itself.
  VectorXd get_residuals(const VectorXd& w,
			 const optional<VectorXi>& batch = none,
			 const optional<VectorXd>& residuals = none) const {
    VectorXd residuals_;

    // if no batch, compute complete residuals
    if(batch == none)
      residuals_ = (residuals == none) ? compute_global_residual_(w) : (*residuals);
    // otherwise only compute a batch of residuals
    else
      residuals_ = (residuals == none) ? compute_batch_residual_(w, *batch) : (*residuals)(*batch);

    return residuals_;
  }

  /// Compute the residual of the Loss for a given record.
  /// @param w Parameters to compute the residual at.
  /// @param i Index of the record.
  /// @param residuals Pre-computed residuals. If not none, just return the
  /// i-th entry of this vector. (This is useful when residuals are updated
  /// dynamically in some optimization algorithms.)
  /// @return Residual if `residuals` is passed. Otherwise `residuals(i)`
  /// itself.
  double get_pointwise_residual(const VectorXd& w,
				const int& i,
				const optional<VectorXd>& residuals) const {
    // if no residual given, compute it
    if (residuals == none)
      return compute_pointwise_residual_(w, i);
    // otherwise just return its precomputed value
    else
      return (*residuals)(i);
  }

  /// Update the residuals after the update of a single coordinate of parameters.
  /// @param residuals Pre-computed residuals. If none the function does nothing.
  /// @param w_update Update to the parameters' coordinate.
  /// @param j Coordinate that has been updated.
  /// @return Updated residuals if `residuals` is not none, none otherwise.
  optional<VectorXd> coord_update_residuals(optional<VectorXd> residuals,
					    const double& w_update=0, const int& j=0) const {

    if(residuals == none)
      return none;
    else
      for(unsigned int i = 0; i < n_; i++)
	(*residuals)(i) = coord_update_pointwise_residual_((*residuals)(i), w_update, i, j);
    return residuals;
  }

  // complete gradient
  /// Compute the gradient of the Loss.
  /// @param w Parameters to compute the gradient at.
  /// @param batch Indices of records to use for computing the gradient. If none,
  /// use all records.
  /// @param clip_threshold Clipping threshold for record-wise gradients. If none,
  /// gradients are not clipped.
  /// @param residuals Pre-computed residuals. If none, residuals are computed.
  /// @return The gradient of the Loss at `w`.
  VectorXd gradient(const VectorXd& w,
		    const optional<VectorXi>& batch = none,
		    const optional<double>& clip_threshold = none,
		    const optional<VectorXd>& residuals = none) const {

    VectorXd grad = VectorXd::Zero(w.size());
    VectorXd residuals_ = get_residuals(w, batch, residuals);

    // if no batch provided, compute complete gradient
    if(batch == none) {
      for(unsigned int i = 0; i < n_; i++) {
	double res_ = get_pointwise_residual(w, i, residuals);

	VectorXd g = pointwise_gradient(w, i, clip_threshold, res_);
	grad += pointwise_gradient(w, i, clip_threshold, res_);
      }
      grad /= n_;
    }
    // otherwise use only selected indices
    else {
      for(unsigned int i = 0; i < (*batch).size(); i++) {
	double res_ = get_pointwise_residual(w, (*batch)(i), residuals);
	grad += pointwise_gradient(w, (*batch)(i), clip_threshold, res_);
      }
      grad /= (*batch).size();
    }

    // return gradient + gradient coming from the regularizer
    return grad + regularizer_->gradient(w);
  }

  /// Compute one entry of the gradient of the Loss.
  /// @param w Parameters to compute the gradient at.
  /// @param j Index of the coordinate to compute.
  /// @param batch Indices of records to use for computing the gradient. If none,
  /// use all records.
  /// @param clip_threshold Clipping threshold for record-wise gradients. If none,
  /// gradients are not clipped.
  /// @param residuals Pre-computed residuals. If none, residuals are computed.
  /// @return The gradient of the Loss at `w`.
  double coord_gradient(const VectorXd& w, const int& j,
			const optional<VectorXi>& batch = none,
			const optional<double>& clip_threshold = none,
			const optional<VectorXd>& residuals = none) const {

    double gradj = 0;
    VectorXd residuals_ = get_residuals(w, batch, residuals);


    // if no batch provided, compute complete coordinate gradient
    if(batch == none) {
      for(unsigned int i = 0; i < n_; i++) {
	double res_ = get_pointwise_residual(w, i, residuals);
	double grad_ = pointwise_coord_gradient(w, i, j, clip_threshold, res_);

	gradj += grad_;
      }
      gradj /= n_;
    }
    // otherwise use only selected indices
    else {
      for(unsigned int i = 0; i < (*batch).size(); i++) {
	double res_ = get_pointwise_residual(w, (*batch)(i), residuals);
	gradj += pointwise_coord_gradient(w, (*batch)(i), j, clip_threshold, res_);
      }
      gradj /= (*batch).size();
    }

    return gradj + regularizer_->coord_gradient(w, j);
  }

  /// Compute one entry of the gradient of the Loss, standardize it (assuming
  /// its mean is m and its standard deviation is s), clip it to one and return it.
  /// @param w Parameters to compute the gradient at.
  /// @param j Index of the coordinate to compute.
  /// @param batch Indices of records to use for computing the gradient. If none,
  /// use all records.
  /// @param m Approximation of the mean of the gradients.
  /// @param s Approximation of the standard deviation of the gradients.
  /// @param residuals Pre-computed residuals. If none, residuals are computed.
  /// @return The standardized gradient of the Loss at `w`.
  double clipped_standardized_coord_gradient(const VectorXd& w, const int& j,
					     const double& m = 0,
					     const double& s = 0,
					     const optional<VectorXi>& batch = none,
					     const optional<VectorXd>& residuals = none) const {

    double s_gradj = 0;
    VectorXd residuals_ = get_residuals(w, batch, residuals);

    double reg_gradj = regularizer_->coord_gradient(w, j);

    // if no batch provided, compute complete coordinate gradient
    if(batch == none) {
      for(unsigned int i = 0; i < n_; i++) {
	double res_ = get_pointwise_residual(w, i, residuals);
	double s_grad_ = pointwise_coord_gradient_(w, i, j, res_) + reg_gradj / n_;

	s_gradj += clip((s_grad_ - m) / s, 1);
      }
      s_gradj /= n_;
    }
    // otherwise use only selected indices
    else {
      int b = (*batch).size();
      for(unsigned int i = 0; i < (*batch).size(); i++) {
	double res_ = get_pointwise_residual(w, (*batch)(i), residuals);
	double s_grad_ = pointwise_coord_gradient_(w, (*batch)(i), j, res_) + reg_gradj / b;

	s_gradj += clip((s_grad_ - m) / s, 1);
      }
      s_gradj /= b;
    }

    return s_gradj;
  }

  /// Compute the gradient of the Loss, standardize it coordinate-wise (assuming
  /// its mean is m and its standard deviation is s), clip each coordinate to one
  /// and return it.
  /// @param w Parameters to compute the gradient at.
  /// @param batch Indices of records to use for computing the gradient. If none,
  /// use all records.
  /// @param m Approximation of the mean of the gradients.
  /// @param s Approximation of the standard deviation of the gradients.
  /// @param residuals Pre-computed residuals. If none, residuals are computed.
  /// @return The standardized gradient of the Loss at `w`.
  VectorXd clipped_standardized_gradient(const VectorXd& w,
					 const VectorXd& m,
					 const VectorXd& s,
					 const optional<VectorXi>& batch = none,
					 const optional<VectorXd>& residuals = none) const {

    VectorXd s_grad = VectorXd::Zero(p_);
    VectorXd residuals_ = get_residuals(w, batch, residuals);

    VectorXd reg_grad = regularizer_->gradient(w);

    // if no batch provided, compute complete coordinate gradient
    if(batch == none) {
      for(unsigned int i = 0; i < n_; i++) {
	VectorXd s_grad_ = pointwise_gradient_(w, i) + reg_grad / n_;

	s_grad += clip((s_grad_ - m).array() / s.array(), 1);
      }
      s_grad /= n_;
    }
    // otherwise use only selected indices
    else {
      int b = (*batch).size();
      for(unsigned int i = 0; i < (*batch).size(); i++) {
	VectorXd s_grad_ = pointwise_gradient_(w, (*batch)(i)) + reg_grad / n_;

	s_grad += clip((s_grad_ - m).array() / s.array(), 1);
      }
      s_grad /= b;
    }

    return s_grad;
  }


  /// Compute the proximal (from regularizer) for a given paramter.
  /// @param w The parameter for which to compute the prox.
  /// @param lr Coefficient of the regularizer. (Think of it as a learning rate.)
  VectorXd prox(const VectorXd& w, const double& lr) const {
    return regularizer_->prox(w, lr);
  }

  /// Compute the proximal (from regularizer) for a coordinate of a given
  /// parameter.
  /// @param w The parameter for which to compute the prox.
  /// @param j Index of the coordinate to compute the prox at.
  /// @param lr Coefficient of the regularizer. (Think of it as a learning rate.)
  double coord_prox(const VectorXd& w, const int& j, const double& lr) const {
    return regularizer_->coord_prox(w, j, lr);
  }

  /// Compute the proximal (from regularizer) for a given (scalar) parameter.
  /// @param w The (scalar) parameter for which to compute the prox.
  /// @param lr Coefficient of the regularizer. (Think of it as a learning rate.)
  double coord_prox(const double& w, const double& lr) const {
    return regularizer_->coord_prox(w, lr);
  }

  /// Evaluate the value of the (regularized) Loss.
  /// @param w Parameter at which to evaluate the Loss.
  /// @param residuals Pre-computed residuals (compute them if none).
  double evaluate(const VectorXd& w, const optional<VectorXd>& residuals = none) const {
    return evaluate_(w, residuals) + regularizer_->evaluate(w);
  }


private:

  // PRIVATE FUNCTIONS TO IMPLEMENT FOR EACH LOSS


  /// (Internal) Evaluate the value of the Loss.
  /// @param w Parameter at which to evaluate the Loss.
  /// @param residuals Pre-computed residuals (compute them if none).
  virtual double evaluate_(const VectorXd&, const optional<VectorXd>& residuals = none) const = 0;

  /// (Internal) Evaluate the value of one coordinate of the gradient for a
  /// given record.
  /// @param w Parameter at which to evaluate the Loss.
  /// @param i Index of the record to use.
  /// @param j Index of the coordinate to compute.
  /// @param residuals Pre-computed residuals (compute them if none).
  virtual double pointwise_coord_gradient_(const VectorXd&,
					   const int&,
					   const int&,
					   const optional<double>& = none) const = 0;

  /// (Internal) Evaluate the value of one coordinate of the gradient for a
  /// given record and clip it.
  /// @param w Parameter at which to evaluate the Loss.
  /// @param i Index of the record to use.
  /// @param j Index of the coordinate to compute.
  /// @param clip_threshold Clipping threshold.
  /// @param residuals Pre-computed residuals (compute them if none).
  virtual double pointwise_coord_gradient(const VectorXd& w,
					  const int& i,
					  const int& j,
					  const optional<double>& clip_threshold = none,
					  const optional<double>& residual = none) const {
    return clip(pointwise_coord_gradient_(w, i, j, residual), clip_threshold);
  }

  /// (Internal) Compute the gradient Lipschitz constant of the non-regularized
  /// Loss.
  virtual double lipschitz_() const = 0;

  /// (Internal) Compute the coordinate gradient Lipschitz constant of the
  /// non-regularized Loss.
  /// @param j Index of the coordinate.
  virtual double coord_lipschitz_(const int&) const = 0;


  /// (Internal) Compute the residuals of the Loss for one record.
  /// @param w Parameters to compute the residuals at.
  /// @param i Record to compute the residual at.
  /// @return Residual of the corresponding record.
  virtual double compute_pointwise_residual_(const VectorXd&, const int&) const = 0;


  /// (Internal) Compute the residuals of the Loss for all records.
  /// @param w Parameters to compute the residuals at.
  /// @return Vector of residuals.
  virtual VectorXd compute_global_residual_(const VectorXd& w) const {
    VectorXd residuals(n_);
    for(unsigned int i = 0; i < n_; i++)
      residuals(i) = compute_pointwise_residual_(w, i);
    return residuals;
  }


  /// (Internal) Compute the residuals of the Loss for a batch of records.
  /// @param w Parameters to compute the residuals at.
  /// @param batch Indices of records for which to compute residuals.
  /// @return Vector of residuals for the relevant indices.
  virtual VectorXd compute_batch_residual_(const VectorXd& w, const VectorXi& batch) const {
    VectorXd residuals(batch.size());
    for(unsigned int i = 0; i < batch.size(); i++)
      residuals(i) = compute_pointwise_residual_(w, batch(i));
    return residuals;
  }


  /// (Internal) Update the residuals after the update of a single coordinate of parameters.
  /// @param residuals Pre-computed residuals. If none the function does nothing.
  /// @param w_update Update to the parameters' coordinate.
  /// @param j Coordinate that has been updated.
  /// @return Updated residuals if `residuals` is not none, none otherwise.
  virtual double coord_update_pointwise_residual_(const double&, const double&, const int&, const int&) const = 0;

  /// (Internal) Update the residuals after the update of the parameters.
  /// @param residuals Pre-computed residuals. If none the function does nothing.
  /// @param w_update Update to the parameters.
  /// @return Updated residuals if `residuals` is not none, none otherwise.
  virtual VectorXd coord_update_global_residual_(const VectorXd& residuals, const double& w_update, const int& j) const {
    VectorXd ret(n_);

    for(unsigned int i = 0; i < n_; i++) {
      ret(i) = coord_update_pointwise_residual_(residuals(i), w_update, i, j);
    }

    return ret;
  }


  /// (Internal) Compute the gradient of the Loss for one record.
  /// @param w Parameters to compute the gradient at.
  /// @param i Index of the record to use.
  /// @param residuals Pre-computed residual for the given record. If none,
  /// residual is computed.
  /// @return The gradient of the Loss at `w` for record `i`.
  VectorXd pointwise_gradient_(const VectorXd& w,
			       const unsigned int& i,
			       const optional<double>& residual = none) const {

    VectorXd gradi(p_);

    // compute each coordinate of the gradient
    for(unsigned int j = 0; j < p_; j++) {
      gradi(j) = pointwise_coord_gradient_(w, i, j, residual);
    }

    // return gradient
    return gradi;
  }

  /// (Internal) Compute the gradient of the Loss for one record.
  /// @param w Parameters to compute the gradient at.
  /// @param i Index of the record to use.
  /// @param clip_threshold Clipping threshold.
  /// @param residuals Pre-computed residual for the given record. If none,
  /// residual is computed.
  /// @return The gradient of the Loss at `w` for record `i`.
  VectorXd pointwise_gradient(const VectorXd& w,
			      const unsigned int& i,
			      const optional<double>& clip_threshold = none,
			      const optional<double>& residual = none) const {
    return clip(pointwise_gradient_(w, i, residual), clip_threshold);
  }

  // GETTERS AND SETTERS
public:
  /// Return the design matrix.
  const MatrixXd& X() const {
    return X_;
  }

  /// Return the labels.
  const VectorXd& y() const {
    return y_;
  }

  /// Return the number of records.
  unsigned int n() const {
    return n_;
  }

  /// Return the number of features.
  unsigned int p() const {
    return p_;
  }

  /// Upper bound on $||X^T X||_2$ (larger absolute eigenvalue).
  double XTX_bound_;

  /// Upper bound on $||X||_\infty$
  double Xinf_bound_;

  /// Upper bound on the $\ell_1$ norm of X's rows.
  double X1_bound_;

  /// Upper bound on the $\ell_2$ norm of X's rows.
  double X2_bound_;

  /// Upper bound on $||y||_\infty$.
  double y_bound_;

  /// Upper bound on the $\ell_2$ norm of X's columns.
  VectorXd X2_feat_bound_;

  /// Upper bound on the $\ell_\infty$ norm of X's columns.
  VectorXd Xinf_feat_bound_;

  /// Memory for lipschitz constant. (Not used I think.)
  double lipschitz_mem_;

  /// Memory for coordinate lipschitz constants. (Not used I think.)
  VectorXd coord_lipschitz_mem_;

  /// Compute all the relevant bounds and lipschitz constants for the Loss,
  /// so that they are computed only once at the beginning.
  void compute_bounds_and_constants()
  {
    // compute global upper bounds that are always useful
    JacobiSVD<MatrixXd> svd(X_);
    XTX_bound_ = std::pow(svd.singularValues().template lpNorm<Eigen::Infinity>(), 2);

    Xinf_feat_bound_ = X_.colwise().template lpNorm<Eigen::Infinity>();
    Xinf_bound_ = Xinf_feat_bound_.template lpNorm<Eigen::Infinity>();
    X1_bound_ = (X_.rowwise().template lpNorm<1>()).template lpNorm<Eigen::Infinity>();
    X2_bound_ = (X_.rowwise().template lpNorm<2>()).template lpNorm<Eigen::Infinity>();
    y_bound_ = y_.template lpNorm<Eigen::Infinity>();
    X2_feat_bound_ = X_.colwise().template lpNorm<2>();
    Xinf_feat_bound_ = X_.colwise().template lpNorm<Eigen::Infinity>();

    for(unsigned int i = 0; i < coord_lipschitz_mem_.size(); i++) {
      coord_lipschitz_mem_(i) = coord_lipschitz(i);
    }
    lipschitz_mem_ = lipschitz();

  }

  /// Value of the parameters at the optimal. (Not used I think.)
  VectorXd opt_coef_;

  /// Value of the parameters at the optimal. (Not used I think.)
  double opt_val_;


protected:
  /// The design Matrix.
  const MatrixXd X_;

  /// The labels.
  const VectorXd y_;

  /// Number of records.
  unsigned int n_;

  /// Number of features.
  unsigned int p_;

  /// Pointer to the regularizer (by default a `NullRegularizer` is build).
  const Regularizer* const regularizer_;

  /// Boolean assessing whether the Loss has build its own regularizer (in which
  /// case it destroys it when desctructor is called).
  bool initialized_regularizer_;

};

#endif
