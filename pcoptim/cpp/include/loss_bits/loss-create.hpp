#ifndef LOSS_CREATE_HEADER_
#define LOSS_CREATE_HEADER_

#include "loss.hpp"
#include "least-squares.hpp"
#include "logistic.hpp"

/// Geneates a loss from a name, data and regularizer.
/// @param name String corresponding to the loss. Use:
///  * `"ls"` for least squares loss $\frac{1}{2n} \norm{Xw - y}_2^2.
///  * `"log"` for logistic loss $\frac{1}{n} \sum_{i=1}^n \log(1 + \exp(-y_ix_i^Tw)$.
/// @param X The design matrix..
/// @param y The labels.
/// @param reg The regularizer.
/// @return A pointer to a Loss object of the right type.
static inline Loss* create_loss(const string& name,
				const MatrixXd& X, const VectorXd& y,
				const Regularizer* const reg) {
  if(name == "ls") {
    return new LeastSquares(X, y, reg);
  }
  else {
    return new Logistic(X, y, reg);
  }
}

#endif
