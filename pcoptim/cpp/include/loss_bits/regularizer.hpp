#ifndef REGULARIZER_HEADER_
#define REGULARIZER_HEADER_

#include <Eigen/Dense>
#include <cmath>
#include <memory>
#include <string>

using Eigen::VectorXd;
using std::string;
using std::shared_ptr;

//____________________________________________________________________________//

// base regularizer class //
class Regularizer {
protected:
  double lambda_;

public:
  // constructor
  Regularizer(const double& lambda) : lambda_(lambda) {}

  virtual ~Regularizer() {}

  // get paramters
  double param() {
    return lambda_;
  }

  void param(const double& lambda) {
    lambda_ = lambda;
  }

  // function evaluation
  virtual double evaluate(const VectorXd&) const = 0;

  // gradients
  VectorXd gradient(const VectorXd& w) const {
    VectorXd ret = VectorXd(w.size());

    for(unsigned int j = 0; j < w.size(); j++) {
      ret[j] = coord_gradient(w, j);
    }

    return ret;
  }

  virtual double coord_gradient(const VectorXd&, const int&) const = 0;

  // lipschitz
  virtual double lipschitz() const = 0;
  virtual double coord_lipschitz(const int&) const = 0;

  // proximal
  VectorXd prox(const VectorXd& w, const double& lr) const {
    VectorXd ret = VectorXd(w.size());

    for(unsigned int j = 0; j < w.size(); j++) {
      ret[j] = coord_prox(w, j, lr);
    }

    return ret;
  }

  double coord_prox(const VectorXd& w, const int& j, const double& lr) const {
    return coord_prox(w(j), lr);
  }

  virtual double coord_prox(const double&, const double&) const = 0;


  // minimum absolute subgradient
  VectorXd min_abs_subgradient(const VectorXd& w, const VectorXd& g) const {
    VectorXd ret = VectorXd(g.size());

    for(unsigned int j = 0; j < g.size(); j++) {
      ret[j] = coord_min_abs_subgradient(w, g, j);
    }

    return ret;
  }

  virtual double coord_min_abs_subgradient(const VectorXd&, const VectorXd& g,
				   const int& j) const {
    return std::abs(g(j));
  }
};

//____________________________________________________________________________//

// null regularizer (ie. no regularization) class
class NullRegularizer : public Regularizer {
public:
  // constructor
  NullRegularizer(const double& lambda=0)
    : Regularizer(lambda) {}

  // function evaluation
  double evaluate(const VectorXd&) const override {
    return 0;
  }

  // gradients
  double coord_gradient(const VectorXd&, const int&) const override {
    return 0;
  }

  // lipschitz
  double lipschitz() const override {
    return 0;
  }

  double coord_lipschitz(const int&) const override {
    return 0;
  }

  // proximal
  double coord_prox(const double& wj, const double&) const override {
    return wj;
  }
};

//____________________________________________________________________________//

// L2 regularization //
class L2Regularizer : public Regularizer {
public:
  // constructor
  L2Regularizer(const double& lambda)
    : Regularizer(lambda) {}

  // function evaluation
  double evaluate(const VectorXd& w) const override {
    return 0.5 * lambda_ *  std::pow(w.norm(), 2);
  }

  // gradients
  double coord_gradient(const VectorXd& w, const int& j) const override {
    return lambda_ * w(j);
  }

  // lipschitz
  double lipschitz() const override {
    return lambda_;
  }

  double coord_lipschitz(const int&) const override {
    return lambda_;
  }

  // proximal
  double coord_prox(const double& wj, const double&) const override {
    return wj;
  }
};

//____________________________________________________________________________//

// L1 regularization //
class L1Regularizer : public Regularizer {
public:
  // constructor
  L1Regularizer(const double& lambda)
    : Regularizer(lambda) {}

  // function evaluation
  double evaluate(const VectorXd& w) const override {
    return lambda_ * w.lpNorm<1>();
  }

  // gradients
  double coord_gradient(const VectorXd&, const int&) const override {
    return 0;
  }

  // lipschitz
  double lipschitz() const override {
    return 0;
  }

  double coord_lipschitz(const int&) const override {
    return 0;
  }

  // proximal
  double coord_prox(const double& wj, const double& lr) const override {
    if(std::abs(wj) < lambda_ * lr)
      return 0;
    else if(wj > 0)
      return wj - lambda_ * lr;
    else
      return wj + lambda_ * lr;
  }

  // min absolute subgradient
  double coord_min_abs_subgradient(const VectorXd& w, const VectorXd& g, const int& j) const override {
    // if w(j) is zero
    // todo: change this absolute constant to something more meaningful
    if(std::abs(w(j)) < 1e-6) {
      if(std::signbit(g(j) - lambda_) != std::signbit(g(j) + lambda_)) {
	return 0;
      }
      else {
	return std::min(std::abs(g(j) - lambda_), std::abs(g(j) + lambda_));
      }
    }

    // if w(j) is not zero
    else {
      return std::abs(g(j)) + lambda_;
    }
  }
};

//____________________________________________________________________________//

// a function that generates appropriate regularizer
static inline Regularizer* create_regularizer(const string& name, const double& param) {
  if(name == "l1") {
    return new L1Regularizer(param);
  }
  else if(name == "l2") {
    return new L2Regularizer(param);
  }
  else {
    return new NullRegularizer(param);
  }
}

#endif
