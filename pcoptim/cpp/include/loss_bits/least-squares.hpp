#ifndef LEASTSQUARES_HEADER_
#define LEASTSQUARES_HEADER_

#include "loss.hpp"
#include "loss-utils.hpp"

class LeastSquares : public Loss {
public:

  LeastSquares(const MatrixXd& X, const VectorXd& y, const Regularizer* reg)
    : Loss(X, y, reg) {

    compute_bounds_and_constants();

  }

  LeastSquares(const MatrixXd& X, const VectorXd& y)
    : Loss(X, y) {

    compute_bounds_and_constants();

  }

  ~LeastSquares() {

  }

  // sensitivity
  double sensitivity(const VectorXd&) const override {
    return 0;
  }

  double coord_sensitivity(const VectorXd&, const int&) const override {
    return 0;
  }



private:
  // function evaluation
  double evaluate_(const VectorXd& w, const optional<VectorXd>& residuals = none) const override {
    VectorXd residuals_ = get_residuals(w, none, residuals);

    return 0.5 / n_ * residuals_.squaredNorm();
  }

  // gradient
  double pointwise_coord_gradient_(const VectorXd& w, const int& i, const int& j,
				   const optional<double>& residual = none) const override {

    // if residual is provided, use itt line, otherwise compute it
    double residuali = (residual == none) ? compute_pointwise_residual_(w, i) : *residual;

    // compute gradient from residual and return it
    return X_(i, j) * residuali;
  }

  // lipschitz
  double lipschitz_() const override {
    return 1.0 / n_ * XTX_bound_;
  }

  double coord_lipschitz_(const int& j) const override {
    return 1.0 / n_ * std::pow(X2_feat_bound_(j), 2);
  }


  // residuals
  double compute_pointwise_residual_(const VectorXd& w,
				     const int& i) const override {
   return X_.row(i) * w - y_(i);
  }

  VectorXd compute_global_residual_(const VectorXd& w) const override {
    return X_ * w - y_;
  }

  VectorXd compute_batch_residual_(const VectorXd& w, const VectorXi& batch) const override {
    return X_(batch, Eigen::all) * w - y_(batch);
  }

  double coord_update_pointwise_residual_(const double& residuali,
					  const double& w_update,
					  const int& i, const int& j) const override {
    return residuali + X_(i, j) * w_update;
  }


};

#endif
