#ifndef LOSS_UTILS_HEADER_
#define LOSS_UTILS_HEADER_

#include "loss.hpp"

/// Convert values of a vector into $\{-1,+1\}$ depending on a given threshold.
/// @param y The vector to convert.
/// @param threshold The threshold at which to cut.
/// @return The discretized vector.
static inline VectorXd discretize(const VectorXd& y, const float& threshold = 0) {
  VectorXd ret(y.size());

  for(unsigned int i = 0; i < y.size(); i++) {
    ret(i) = y(i) > threshold ? 1 : -1;
  }

  return ret;
}

#endif
