#ifndef LOGISTIC_HEADER_
#define LOGISTIC_HEADER_

#include "loss.hpp"
#include "loss-utils.hpp"

class Logistic : public Loss {
public:

  Logistic(const MatrixXd& X, const VectorXd& y, const Regularizer* reg)
    : Loss(X, discretize(y), reg) {

    compute_bounds_and_constants();

  }

  Logistic(const MatrixXd& X, const VectorXd& y)
    : Loss(X, discretize(y)) {

    compute_bounds_and_constants();

  }

  ~Logistic() {

  }

  // residual computation
  double compute_pointwise_residual_(const VectorXd& w,
				     const int& i) const override {
    return y_(i) * X_.row(i).dot(w);
  }


  VectorXd compute_global_residual_(const VectorXd& w) const override {
    return y_.array() * (X_ * w).array();
  }

  VectorXd compute_batch_residual_(const VectorXd& w, const VectorXi& batch) const override {
    return y_(batch).array() * (X_(batch, Eigen::all) * w).array();
  }

  double coord_update_pointwise_residual_(const double& residuali,
					  const double& w_update,
					  const int& i, const int& j) const override {
    return residuali + y_(i) * X_(i, j) * w_update;
  }



  // sensitivity
  double sensitivity(const VectorXd&) const override {
    return 0;
  }

  double coord_sensitivity(const VectorXd&, const int&) const override {
    return 0;
  }

private:

  // function evaluation
  double evaluate_(const VectorXd& w, const optional<VectorXd>& residuals = none) const override {
    VectorXd residuals_ = (residuals == none) ? get_residuals(w) : *residuals;
    double obj = 0;

    for(unsigned int i = 0; i < n_; i++) {
      obj += std::log(1 + std::exp(- residuals_(i)));
    }

    return obj / n_;
  }

  // gradient
  double pointwise_coord_gradient_(const VectorXd& w, const int& i, const int& j,
				   const optional<double>& residual = none) const override {

    // if residual is provided, use itt line, otherwise compute it
    double residuali = (residual == none) ? compute_pointwise_residual_(w, i) : *residual;

    // compute gradient from residual and return it
    return - y_(i) / (1.0 + std::exp(residuali)) * X_(i, j);
  }

  // lipschitz
  double lipschitz_() const override {
    return 0.25 / n_ * XTX_bound_;
  }

  double coord_lipschitz_(const int& j) const override {
    return 0.25 / n_ * std::pow(X2_feat_bound_(j), 2);
  }
};

#endif
