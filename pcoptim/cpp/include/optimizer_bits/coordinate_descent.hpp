#pragma once
#include <Eigen/Dense>

#include <boost/none_t.hpp>
#include <boost/optional.hpp>
#include <boost/range/adaptor/indexed.hpp>
#include <optional>
#include <string>
#include <random>
#include <stdexcept>
#include <memory>

#include <iostream>


#include "../loss"
#include "../utils/logs.hpp"
#include "../utils/noise_utils.hpp"
#include "../utils/privacy_utils.hpp"
#include "batch.hpp"

using Eigen::MatrixXd;
using Eigen::VectorXd;
using Eigen::VectorXi;

using boost::optional;
using boost::none;
using std::shared_ptr;
using std::string;

/// Run coordinate descent on a given loss.
Log coordinate_descent(const Loss* loss,
		       const VectorXd& w0,
		       const unsigned int& max_iter=100,
		       const double& learning_rate=1.0,
		       const int& nb_logs=10,
		       const string& strategy="cycle",
		       const double& alpha_sample=0.0,
		       const optional<double>& clip=none,
		       const double& alpha_clip=1.0,
		       //		       const string clip_strategy="lipschitz",
		       const optional<int>& batch_size=none,
		       const string batch_strategy="shuffle",
		       const optional<VectorXd>& coord_lip=none,
		       const double& epsilon=0,
		       double delta=-1,
		       const int& q=2,
		       int seed=-1,
		       const bool& epochs=true) {

  // compute appropriate number of iterations
  unsigned int nb_iter = max_iter;
  if(epochs) {
    if(batch_size != none)
      nb_iter *= loss->n() / (*batch_size);
    nb_iter *= loss->p();
  }

  // initialization
  Log logs(nb_iter, nb_logs);
  VectorXd w = w0;
  VectorXd lr(loss->p());
  for(unsigned int j = 0; j < loss->p(); j++) {
    if(coord_lip == none)
      lr(j) = learning_rate / loss->coord_lipschitz(j);
    else
      lr(j) = learning_rate / (*coord_lip)(j);
  }

  // x step length
  double xstep = (batch_size == none)
    ? 1.0 / loss->p()
    : (double)*batch_size / (loss->p() * loss->n());

  // residuals
  optional<VectorXd> residuals = none;
  if(batch_size == none) residuals = loss->get_residuals(w);

  // random number generator
  std::default_random_engine generator(seed);

  // random batch generator
  optional<VectorXi> batch_indices = boost::make_optional(false, VectorXi());
  BatchGenerator* rbg = create_batch_generator(loss->n(), batch_strategy, generator);
  double subsampling_ratio = (batch_size == none) ? -1 : (double)(*batch_size) / loss->n();

  // clipping thresholds
  vector< optional<double> > clipping_thresholds(loss->p(), none);
  if(clip != none) {
    // compute lipschitz constants and raise them to power alpha_clip
    // if the constants are provided as argument, use them
    VectorXd lipalpha = (coord_lip == none) ?
      loss->get_all_lipschitz_coord()
      : *coord_lip;

    for(unsigned int j = 0; j < loss->p(); j++) {
      lipalpha(j) = std::pow(lipalpha(j), alpha_clip);
    }

    // compute thresholds
    // if alpha_clip = 0, clip uniformly to 1/sqrt(p)
    // else adapt to coord gradient lipschitz constants
    double lipsum = lipalpha.sum();
    for(unsigned int j = 0; j < loss->p(); j++) {
      clipping_thresholds[j] = (*clip) * std::sqrt(lipalpha(j) / lipsum);
    }
  }

  // privacy needs
  VectorXd scale = VectorXd::Zero(loss->p());
  if(epsilon > 0) {
    // if no clipping provided, return immediately
    if(clip == none) {
      throw std::invalid_argument("coordinate_descent: if epsilon > 0, clipping must be provided.");
    }

    // default delta if not provided
    if(delta <= 0) delta = 1.0 / pow(loss->n(), 2);

    // compute scale if algorithm is noisy
    double priv_constant = best_privacy_constant_rdp(nb_iter, epsilon, delta, subsampling_ratio);
    //    std::cout << "CD - priv constant : " << priv_constant << std::endl;

    for(unsigned int j = 0; j < loss->p(); j++) {
      scale(j) = priv_constant * 2 * (*(clipping_thresholds[j])) / (loss->n() * subsampling_ratio);
	//	priv_constant * std::sqrt(nb_iter * std::log(1.0/delta))
	//	* (*(clipping_thresholds[j])) / (loss->n() * epsilon);
    }
  }

  // initialize sampling distribution
  // compute lipschitz constants and raise them to power alpha_sample
  VectorXd lipsample = loss->get_all_lipschitz_coord();
  for(unsigned int j = 0; j < loss->p(); j++) {
    lipsample(j) = std::pow(lipsample(j), alpha_sample);
  }
  std::discrete_distribution<> index_sample_dist(lipsample.begin(), lipsample.end());

  // choice of updated parameter
  int j = 0;

  // log initial values
  logs.init_timer();
  logs.write(0, loss->evaluate(w));


  for(unsigned int t = 0; t < nb_iter + 1; t++) {

    // get new batch
    if(batch_size != none) batch_indices = rbg->get_batch(*batch_size);

    // compute next coordinate
    if(strategy == "cycle") j = (j+1) % loss->p();
    else j = index_sample_dist(generator); // random_int(0, loss->p(), generator);

    // remember old value of wj
    double wj_old = w(j);

    // compute gradient
    double coord_gradient = loss->coord_gradient(w, j, batch_indices, clipping_thresholds[j], residuals);

    // compute noise
    double noise = zero_mean_noise(scale(j), q, generator);

    // update
    w(j) = loss->coord_prox(w(j) - lr(j) * (coord_gradient + noise), lr(j));

    // update residual
    if(batch_size == none) residuals = loss->coord_update_residuals(residuals, w(j) - wj_old, j);

    // log current values (if needed at this step)
    if(logs.should_log(t)) {
      logs.write((double)t * xstep, loss->evaluate(w, residuals));
    }
  }

  logs.final_coef = w;
  logs.final_obj = loss->evaluate(w);

  delete rbg;
  return logs;
}
