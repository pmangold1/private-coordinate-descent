#pragma once
#include <Eigen/Dense>

#include <boost/none_t.hpp>
#include <boost/optional.hpp>
#include <boost/range/adaptor/indexed.hpp>
#include <optional>
#include <string>
#include <random>
#include <stdexcept>
#include <memory>

#include <iostream>


#include "../loss"
#include "../utils/logs.hpp"
#include "../utils/noise_utils.hpp"
#include "../utils/privacy_utils.hpp"
#include "batch.hpp"

using Eigen::MatrixXd;
using Eigen::VectorXd;
using Eigen::VectorXi;

using boost::optional;
using boost::none;
using std::shared_ptr;
using std::string;

/// Run coordinate descent on a given loss.
Log adaclip_gradient_descent(const Loss* loss,
			     const VectorXd& w0,
			     const unsigned int& max_iter = 100,
			     const double& learning_rate = 1.0,
			     const int& nb_logs = 10,
			     const double& beta1 = 0.99,
			     const double& beta2 = 0.9,
			     const double& h1 = 1e-12,
			     const double& h2 = 1,
			     const optional<int>& batch_size = none,
			     const string batch_strategy = "shuffle",
			     const double& epsilon=0,
			     double delta=-1,
			     const int& q=2,
			     int seed=-1,
			     const bool& epochs=true) {

  // compute appropriate number of iterations
  unsigned int nb_iter = max_iter;
  if(epochs) {
    if(batch_size != none)
      nb_iter *= loss->n() / (*batch_size);
  }

  // initialization
  Log logs(nb_iter, nb_logs);
  VectorXd w = w0;
  double lr = learning_rate / loss->lipschitz();

  // x step length
  double xstep = (batch_size == none)
    ? 1.0
    : (double)*batch_size / loss->n();

  // random number generator
  std::default_random_engine generator(seed);

  // random batch generator
  optional<VectorXi> batch_indices = boost::make_optional(false, VectorXi());
  BatchGenerator* rbg = create_batch_generator(loss->n(), batch_strategy, generator);
  double subsampling_ratio = (batch_size == none) ? -1 : (double)(*batch_size) / loss->n();

  // clipping thresholds
  VectorXd m = VectorXd::Zero(loss->p());
  VectorXd s = VectorXd::Constant(loss->p(), std::sqrt(h1 * h2));
  VectorXd b, v, w_tilde, g;

  // noise scale for 2-sensitivity function
  double sigma = 0;
  if(epsilon > 0) {

    // default delta if not provided
    if(delta <= 0) delta = 1.0 / pow(loss->n(), 2);

    if(batch_size == none)
      sigma = 2 * best_privacy_constant_rdp(nb_iter, epsilon, delta, subsampling_ratio) / loss->n();
    else
      sigma = 2 * best_privacy_constant_rdp(nb_iter, epsilon, delta, subsampling_ratio) / *batch_size;

  }

  // log initial values
  logs.init_timer();
  logs.write(0, loss->evaluate(w));


  for(unsigned int t = 0; t < nb_iter + 1; t++) {

    // update b (noise scales vector)
    b = std::sqrt(s.sum()) * s.cwiseSqrt();

    // get new batch
    if(batch_size != none) batch_indices = rbg->get_batch(*batch_size);

    // compute standardized gradient (clipped to 1)
    w_tilde = loss->clipped_standardized_gradient(w, m, b, batch_indices);

    // compute noise and obfuscate w
    if(sigma > 0) {
      for(unsigned int j=0; j < loss->p(); j++) {
	w_tilde(j) += zero_mean_noise(sigma, q, generator);
      }
    }

    // compute private gradient estimator
    g = b.cwiseProduct(w_tilde) + m;

    // update
    w = loss->prox(w - lr * g, lr);

    // update clipping stuff
    m = beta1 * m + (1 - beta1) * g;
    v = (g - m).cwiseProduct(g - m) - sigma * sigma * b.cwiseProduct(b);

    for(unsigned int j = 0; j < loss->p(); j++) {
      if(v(j) < h1) v(j) = h1;
      if(v(j) > h2) v(j) = h2;
    }

    s = (beta2 * s.cwiseProduct(s) + (1 - beta2) * v).cwiseSqrt();

    // log current values (if needed at this step)
    if(logs.should_log(t)) {
      logs.write((double)t * xstep, loss->evaluate(w));
    }
  }

  logs.final_coef = w;
  logs.final_obj = loss->evaluate(w);

  delete rbg;
  return logs;
}
