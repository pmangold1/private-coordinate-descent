#pragma once
#include <Eigen/Dense>
#include <algorithm>

#include "../utils/noise_utils.hpp"

using Eigen::VectorXi;
using Eigen::seq;
using Eigen::last;

/// Return a vector containing all integers between two integers.
/// @param RNG Type of the random number generator.
/// @param b Beginning of the interval (included).
/// @param e End the interval (not included).
/// @param shuffle Shuffle vector before returning it if this parameters is `true`.
/// @param generator A random number generator.
template< class RNG >
VectorXi linear_integers(const int& b, const int& e, const bool& shuffle, RNG& generator) {
  VectorXi v(e - b);

  for(unsigned int i = 0; i < v.size(); i++) {
    v(i) = b + i;
  }

  if(shuffle) {
    std::shuffle(v.data(), v.data() + v.size(), generator);
  }

  return v;
}


class BatchGenerator {
protected:
  /// Return a vector of indices.
  virtual VectorXi get_more_indices_() = 0;

  /// Maximal value of an index.
  unsigned int n_;

  /// Memory for precomputed indices.
  VectorXi precomputed_batch_;

  /// Position in the precomputed indices.
  unsigned int pos_;

public:
  /// Constructor.
  /// @param n Maximal index.
  BatchGenerator(const unsigned int& n)
    : n_(n), pos_(0) {

  }

  /// Constructor.
  /// @param n Maximal index.
  /// @param v Precomputed indices.
  BatchGenerator(const unsigned int& n, const VectorXi& v)
    : n_(n), precomputed_batch_(v), pos_(0) {

  }

  /// Destructor.
  virtual ~BatchGenerator() {

  }

  /// Return the next `batch_size` indices.
  /// @param batch_size Number of indices to return.
  /// @return A batch of indices.
  VectorXi get_batch(const unsigned int& batch_size) {

    if(precomputed_batch_.size() - pos_ < batch_size) {
      populate_();
    }

    // get next batch_size elements
    VectorXi batch_indices = precomputed_batch_(seq(pos_, pos_ + batch_size - 1));

    // update position in vector
    pos_ += batch_size;

    return batch_indices;
  }

private:
  /// Populate internal memory with the appropriate values.
  void populate_() {
    // get remaining indices and generate more
    VectorXi unused_indices = precomputed_batch_(seq(pos_, last));
    VectorXi new_indices = get_more_indices_();

    // reset position
    pos_ = 0;

    // put in memory
    precomputed_batch_.resize(unused_indices.size() + new_indices.size());
    precomputed_batch_ << unused_indices, new_indices;
  }
};

template< class RNG >
class RandomBatchGenerator : public BatchGenerator {
public:
  RandomBatchGenerator(const unsigned int& n, RNG& generator)
    : BatchGenerator(n, random_int_vector(n, 0, n, generator)),
      generator_(generator) {

  }

  ~RandomBatchGenerator() {

  }

protected:
  VectorXi get_more_indices_() override {
    return random_int_vector(n_, 0, n_, generator_);
  }

  RNG& generator_;
};

template< class RNG >
class SequentialBatchGenerator : public BatchGenerator {
public:
  SequentialBatchGenerator(const unsigned int& n, RNG& generator, const bool& shuffle=false)
    : BatchGenerator(n, linear_integers(0, n, shuffle, generator)),
      shuffle_(shuffle),
      generator_(generator) {

  }

  ~SequentialBatchGenerator() {

  }

protected:
  VectorXi get_more_indices_() override {
    return linear_integers(0, n_, shuffle_, generator_);
  }

  bool shuffle_;
  RNG& generator_;
};


/// Geneates a batch generator from a name.
/// @param RNG Type of the random number generator.
/// @param n Maximal index.
/// @param bstrategy Strategy for batch generation:
///  * `"shuffle"` for randomly shuffled [0,n-1].
///  * `"sequential"` for ordered [0,n-1].
///  * anything else for uniformly randomly generated indices.
/// @param X The design matrix..
/// @param y The labels.
/// @param reg The regularizer.
/// @return A pointer to a Loss object of the right type.
template< class RNG >
BatchGenerator* create_batch_generator(const int& n, const string& bstrategy, RNG& generator) {
  if(bstrategy == "shuffle") {
    return new SequentialBatchGenerator<RNG>(n, generator, true);
  }
  else if(bstrategy == "sequential") {
    return new SequentialBatchGenerator<RNG>(n, generator, false);
  }
  else {
    return new RandomBatchGenerator<RNG>(n, generator);
  }
}
