#pragma once
#include <Eigen/Dense>

#include <boost/none_t.hpp>
#include <boost/optional.hpp>
#include <boost/range/adaptor/indexed.hpp>
#include <optional>
#include <string>
#include <random>
#include <stdexcept>
#include <memory>

#include <iostream>


#include "../loss"
#include "../utils/logs.hpp"
#include "../utils/noise_utils.hpp"
#include "../utils/privacy_utils.hpp"
#include "batch.hpp"

using Eigen::MatrixXd;
using Eigen::VectorXd;
using Eigen::VectorXi;

using boost::optional;
using boost::none;
using std::shared_ptr;
using std::string;

/// Run coordinate descent on a given loss.
Log adaclip_coordinate_descent(const Loss* loss,
			       const VectorXd& w0,
			       const unsigned int& max_iter = 100,
			       const double& learning_rate = 1.0,
			       const int& nb_logs = 10,
			       const string& strategy = "cycle",
			       const double& alpha_sample = 0.0,
			       const double& beta1 = 0.99,
			       const double& beta2 = 0.9,
			       const double& h1 = 1e-12,
			       const double& h2 = 1,
			       //		       const string clip_strategy = "lipschitz",
			       const optional<int>& batch_size = none,
			       const string batch_strategy = "shuffle",
			       const double& epsilon=0,
			       double delta=-1,
			       const int& q=2,
			       int seed=-1,
			       const bool& epochs=true) {

  // compute appropriate number of iterations
  unsigned int nb_iter = max_iter;
  if(epochs) {
    if(batch_size != none)
      nb_iter *= loss->n() / (*batch_size);
    nb_iter *= loss->p();
  }

  // initialization
  Log logs(nb_iter, nb_logs);
  VectorXd w = w0;
  VectorXd lr(loss->p());
  for(unsigned int j = 0; j < loss->p(); j++) {
    lr(j) = learning_rate / loss->coord_lipschitz(j);
  }

  VectorXd lip_const = loss->get_all_lipschitz_coord();

  // x step length
  double xstep = (batch_size == none)
    ? 1.0 / loss->p()
    : (double)*batch_size / (loss->p() * loss->n());

  // residuals
  optional<VectorXd> residuals = none;
  if(batch_size == none) residuals = loss->get_residuals(w);

  // random number generator
  std::default_random_engine generator(seed);

  // random batch generator
  optional<VectorXi> batch_indices = boost::make_optional(false, VectorXi());
  BatchGenerator* rbg = create_batch_generator(loss->n(), batch_strategy, generator);
  double subsampling_ratio = (batch_size == none) ? -1 : (double)(*batch_size) / loss->n();

  // clipping thresholds
  VectorXd m = VectorXd::Zero(loss->p());
  VectorXd s = VectorXd::Constant(loss->p(), std::sqrt(h1 * h2));

  double v, w_tilde, g;

  // noise scale for 2-sensitivity function
  double sigma = 0;
  if(epsilon > 0) {

    // default delta if not provided
    if(delta <= 0) delta = 1.0 / pow(loss->n(), 2);
    sigma = 2 * best_privacy_constant_rdp(nb_iter, epsilon, delta, subsampling_ratio) / loss->n();

  }


  // initialize sampling distribution
  // compute lipschitz constants and raise them to power alpha_sample
  VectorXd lipsample = loss->get_all_lipschitz_coord();
  for(unsigned int j = 0; j < loss->p(); j++) {
    lipsample(j) = std::pow(lipsample(j), alpha_sample);
  }

  std::discrete_distribution<> index_sample_dist(lipsample.begin(), lipsample.end());

  int j = 0;


  // log initial values
  logs.init_timer();
  logs.write(0, loss->evaluate(w));


  for(unsigned int t = 0; t < nb_iter + 1; t++) {

    // get new batch
    if(batch_size != none) batch_indices = rbg->get_batch(*batch_size);

    // compute next coordinate
    if(strategy == "cycle") j = (j+1) % loss->p();
    else j = index_sample_dist(generator); // random_int(0, loss->p(), generator);


    // remember old value of wj
    double wj_old = w(j);

    // compute gradient
    //    true_g = loss->coord_gradient(w, j, batch_indices, clipping_thresholds[j], residuals);

    // compute standardized gradient (clipped to 1)
    w_tilde = loss->clipped_standardized_coord_gradient(w, j, m(j), s(j),
							batch_indices, residuals);

    // compute noise and obfuscate w
    if(sigma > 0) {
      double noise = zero_mean_noise(sigma, q, generator);
      w_tilde += noise;
    }


    g = s(j) * w_tilde + m(j);

    // update
    w(j) = loss->coord_prox(w(j) - lr(j) * g, lr(j));

    // update residual
    if(batch_size == none) residuals = loss->coord_update_residuals(residuals, w(j) - wj_old, j);

    // update clipping stuff
    m(j) = beta1 * m(j) + (1 - beta1) * g;
    v = (g - m(j)) * (g - m(j)) - s(j) * s(j) * sigma * sigma;

    if(v < h1) v = h1;
    if(v > h2 * lip_const(j)) v = h2 * lip_const(j);

    s(j) = std::sqrt(beta2 * s(j) * s(j) + (1 - beta2) * v);

    // log current values (if needed at this step)
    if(logs.should_log(t)) {
      logs.write((double)t * xstep, loss->evaluate(w, residuals));
    }
  }

  logs.final_coef = w;
  logs.final_obj = loss->evaluate(w);

  delete rbg;
  return logs;
}
