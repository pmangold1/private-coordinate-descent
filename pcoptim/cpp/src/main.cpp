#include <iostream>
#include <string>
#include <eigen/Dense>
#include <boost/program_options.hpp>
#include <boost/optional/optional_io.hpp>
#include <memory>
#include <iomanip>
#include <experimental/filesystem>

#include "../h/regularizer.hpp"
#include "../h/loss.hpp"
#include "../h/logs.hpp"
#include "../h/coordinate_descent.hpp"
#include "../h/gradient_descent.hpp"
#include "../h/data_utils.hpp"

using Eigen::Matrix2d;
using Eigen::VectorXd;
using std::string;
using std::vector;

using boost::optional;
using boost::none;
using std::ofstream;

namespace po = boost::program_options;

int main(int argc, char* argv[]) {
  std::random_device r;

  // Declare the supported options.
  po::options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("T", po::value<int>(), "number of iterations")
    ("clip", po::value< optional<double> >(), "clipping value")
    //("clipstrategy", po::value<string>(), "clipping strategy (prop to lipschitz or uniform)")
    ("alpha_clip", po::value<double>(), "clipping strategy (1 = prop to lipschitz, 0 = uniform)")
    ("lr", po::value<double>(), "learning rate (scaled with smoothness constants)")
    ("nlog", po::value<int>(), "number of logs")
    ("b", po::value< optional<int> >(), "batch size")
    ("bstrategy", po::value<string>(), "strategy for coordinate descent")
    ("if", po::value< optional<string> >(), "input file")
    ("od", po::value< optional<string> >(), "output directory")
    ("eps", po::value<double>(), "epsilon (dp)")
    ("delta", po::value<double>(), "delta (dp)")
    ("q", po::value<unsigned int>(), "privacy mechanism 2 = normal, any other = laplace (default normal)")
    ("seed", po::value<int>(), "random number generator seed")
    ("algo", po::value<string>(), "algorithm to use")
    ("r", po::value<string>(), "regularizer to use")
    ("rparam", po::value<double>(), "regularization parameter (defaults to 1.0)")
    ("loss", po::value<string>(), "loss to use")
    ("cstrategy", po::value<string>(), "strategy for coordinate descent")
    ("alpha_sample", po::value<double>(), "power of the lipschitz constant in sampling (1.0 = prop to lipschitz, 0.0 = uniform) not used if cstrategy='cyclic'")
    ("nrun", po::value<int>(), "number of runs of algorithm")
    ("w0", po::value<double>(), "initial value of w0 (uses a vector full of this value as start)")
    ;

  po::variables_map vm;
  po::store (po::command_line_parser (argc, argv).options (desc)
	     .style (po::command_line_style::default_style |
		     po::command_line_style::allow_long_disguise)
	     .run (), vm);
  po::notify(vm);


  // get argument values
  unsigned int max_iter = vm.count("T") ? vm["T"].as<int>() : 100;
  double learning_rate = vm.count("lr") ? vm["lr"].as<double>() : 1.0;
  unsigned int nb_logs = vm.count("nlog") ? vm["nlog"].as<int>() : max_iter;
  double epsilon = vm.count("eps") ? vm["eps"].as<double>() : -1;
  double delta = vm.count("delta") ? vm["delta"].as<double>() : -1;
  unsigned int q = vm.count("q") ? vm["q"].as<unsigned int>() : 2;
  int seed = vm.count("seed") ? vm["seed"].as<int>() : r();
  string algo = vm.count("algo") ? vm["algo"].as<string>() : "gd";
  string loss_name = vm.count("loss") ? vm["loss"].as<string>() : "ls";
  string reg_name = vm.count("r") ? vm["r"].as<string>() : "null";
  double reg_param = vm.count("rparam") ? vm["rparam"].as<double>() : 1.0;
  string bstrategy = vm.count("bstrategy") ? vm["bstrategy"].as<string>() : "shuffle";
  string cstrategy = vm.count("cstrategy") ? vm["cstrategy"].as<string>() : "cycle";
  double alpha_clip = vm.count("alpha_clip") ? vm["alpha_clip"].as<double>() : 0.0;
  optional<double> clip = vm.count("clip") ? vm["clip"].as< optional<double> >() : none;
  double alpha_sample = vm.count("alpha_sample") ? vm["alpha_sample"].as<double>() : 1.0;
  optional<int> batch_size = vm.count("b") ? vm["b"].as< optional<int> >() : none;
  optional<string> odir = vm.count("od") ? vm["od"].as< optional<string> >() : none;
  optional<string> ifpath = vm.count("if") ? vm["if"].as< optional<string> >() : none;
  unsigned int nb_runs = vm.count("nrun") ? vm["nrun"].as<int>() : 1;
  double w0_value = vm.count("w0") ? vm["w0"].as<double>() : 0;


  if (vm.count("help")) {
    std::cout << desc << "\n";
    return 1;
  }

  // fetch data
  // read data
  if(ifpath == none) {
    std::cout << "please provide some data (-if <dataset.csv>)." << std::endl;
    return -1;
  }

  vector<string> header;
  MatrixXd data = read_csv<MatrixXd>(*ifpath, header);
  MatrixXd X = drop_column(data, header, "y");
  VectorXd y = get_target(data, header, "y");
  VectorXd w0 = w0_value * VectorXd::Ones(X.cols());

  // loss
  Regularizer* reg = create_regularizer(reg_name, reg_param);
  Loss* loss = create_loss(loss_name, X, y, reg);

  // default delta value if epsilon is provided
  if(epsilon > 0) {
    delta = 1.0 / std::pow(loss->n(), 2);
  }

  string ofpath;
  // run gradient descent
  ofstream of;
  if(odir != none) {
    // create directory if not existing yet
    std::experimental::filesystem::create_directory(*odir);

    // generate path to file
    ofpath = format_file_name(*odir, algo, loss_name, reg_name, reg_param,
			      max_iter, learning_rate, epsilon, delta,
			      q, (batch_size == none ? -1 : *batch_size),
			      bstrategy, cstrategy, alpha_sample,
			      (clip == none ? -1 : *clip), alpha_clip, nb_runs);

    // write parameters in file (even if redundant with name...)
    of.open(ofpath);
    of << "{\"params\": {"
       << "\"algo\": \"" << algo
       << "\", \"dataset\": \"" << *ifpath
       << "\", \"loss\": \"" << loss_name
       << "\", \"regularizer\": \"" << reg_name
       << "\", \"regularizer_parameter\": " << reg_param
       << ", \"max_iter\": " << max_iter
       << ", \"learning_rate\": " << learning_rate
       << ", \"epsilon\": " << epsilon
       << ", \"delta\": " << delta
       << ", \"q\": " << q
       << ", \"batch_strategy\": \"" << bstrategy
       << "\", \"batch_size\": " << (batch_size == none ? -1 : *batch_size)
       << ", \"coord_strategy\": \"" << cstrategy
       << "\", \"clip\": " << (clip == none ? -1 : *clip)
       << ", \"alpha_clip\": " << alpha_clip
       << ", \"alpha_sample\": " << alpha_sample
       << ", \"nb_runs\": " << nb_runs
       << "},\n \"result\": [";
  }

  for(unsigned int run = 0; run < nb_runs; run++) {
    Log logs;
    if(algo == "gd") {
      logs = gradient_descent(loss, w0, max_iter, learning_rate, nb_logs,
			      clip, batch_size, bstrategy, epsilon, delta, q, seed + run);
    }
    else {
      logs = coordinate_descent(loss, w0, max_iter, learning_rate, nb_logs,
				cstrategy, alpha_sample, clip, alpha_clip, // clipstrategy,
				batch_size, bstrategy,
				epsilon, delta, q, seed + run);
    }

    // show objective
    // logs.print_obj();

    // write to file if argument is provided
    if(odir != none) {
      logs.write_to_file(of);

      if(run < nb_runs - 1) of << "," << std::endl;
    }

    std::cout << "[Run " << run << "] Objective : "
	      << std::fixed << std::setprecision(15) << logs.final_obj << std::endl;
  }

  if(odir != none) {
    of << "]}" << std::endl;
    of.close();
  }


  delete loss;
  delete reg;
  return 0;
}
