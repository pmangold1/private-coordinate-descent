#include <catch2/catch.hpp>
#include <Eigen/Dense>
#include <iostream>

#include "../include/loss"


using Eigen::MatrixXd;
using Eigen::VectorXd;


/*
________________________________________________________________________________

  LOGISTIC
________________________________________________________________________________

 */

TEST_CASE( "LOGISTIC LOSS", "[loss]" ) {

  MatrixXd X {
    {1, 2, 3},
    {4, 5, 6}
  };
  VectorXd y {{ 1, -1 }};
  VectorXd w {{ 1, 2, 3 }};

  Logistic log(X, y);

  // functions evaluation
  REQUIRE( log.evaluate(w) == Approx(16.000000415764195) );

  // gradients
  VectorXd grad = log.gradient(w);
  VectorXd true_grad {{ 1.99999958, 2.49999917, 2.99999875 }};
  for(unsigned int i = 0; i < grad.size(); i++) {
    REQUIRE( log.coord_gradient(w, i) == grad(i) );
    REQUIRE( grad(i) == Approx(true_grad(i)) );
  }

  // problem dimensions
  REQUIRE( log.n() == 2 );
  REQUIRE( log.p() == 3 );

  // residuals
  VectorXd residuals = log.get_residuals(w);
  VectorXd true_residuals {{ 14, -32 }};

  for(unsigned int i = 0; i < residuals.size(); i++) {
    REQUIRE( log.get_pointwise_residual(w, i, residuals) == residuals(i) );
    REQUIRE( residuals(i) == Approx(true_residuals(i)) );
  }

  VectorXd w_updated {{ 2, 2, 3 }};
  REQUIRE( *log.coord_update_residuals(residuals, 1, 0) == log.get_residuals(w_updated) );


  // lipschitz constants
  VectorXd coord_lip = log.get_all_lipschitz_coord();
  VectorXd true_lip {{ 2.125, 3.6249999999999996, 5.625000000000001 }};
  for(unsigned int i = 0; i < coord_lip.size(); i++) {
    REQUIRE( coord_lip(i) == log.coord_lipschitz(i) );
    REQUIRE( coord_lip(i) == Approx(true_lip(i)) );
  }
  REQUIRE( log.lipschitz() == Approx(11.30033406578173) );

  // proximal
  VectorXd prox = log.prox(w, 10);
  for(unsigned int i = 0; i < log.p(); i++) {
    REQUIRE( log.coord_prox(w, i, 10) == prox(i) );
    REQUIRE( prox(i) == w(i) );
  }

}


/*
________________________________________________________________________________

  LOGISTIC + L1 Reg
________________________________________________________________________________

 */

TEST_CASE( "LOGISTIC + L1 LOSS", "[loss]" ) {

  MatrixXd X {
    {1, 2, 3},
    {4, 5, 6}
  };
  VectorXd y {{ 1, -1 }};
  VectorXd w {{ 1, 2, 3 }};

  L1Regularizer reg(1.0);
  Logistic log(X, y, &reg);

  // functions evaluation
  REQUIRE( log.evaluate(w) == Approx(22.000000415764195) );

  // gradients
  VectorXd grad = log.gradient(w);
  VectorXd true_grad {{ 1.99999958, 2.49999917, 2.99999875 }};
  for(unsigned int i = 0; i < grad.size(); i++) {
    REQUIRE( log.coord_gradient(w, i) == grad(i) );
    REQUIRE( grad(i) == Approx(true_grad(i)) );
  }

  // problem dimensions
  REQUIRE( log.n() == 2 );
  REQUIRE( log.p() == 3 );

  // residuals
  VectorXd residuals = log.get_residuals(w);
  VectorXd true_residuals {{ 14, -32 }};

  for(unsigned int i = 0; i < residuals.size(); i++) {
    REQUIRE( log.get_pointwise_residual(w, i, residuals) == residuals(i) );
    REQUIRE( residuals(i) == Approx(true_residuals(i)) );
  }

  VectorXd w_updated {{ 2, 2, 3 }};
  REQUIRE( *log.coord_update_residuals(residuals, 1, 0) == log.get_residuals(w_updated) );


  // lipschitz constants
  VectorXd coord_lip = log.get_all_lipschitz_coord();
  VectorXd true_lip {{ 2.125, 3.6249999999999996, 5.625000000000001 }};
  for(unsigned int i = 0; i < coord_lip.size(); i++) {
    REQUIRE( coord_lip(i) == log.coord_lipschitz(i) );
    REQUIRE( coord_lip(i) == Approx(true_lip(i)) );
  }
  REQUIRE( log.lipschitz() == Approx(11.30033406578173) );

  // proximal
  VectorXd prox = log.prox(w, 2);
  VectorXd true_prox {{ 0, 0, 1 }};
  for(unsigned int i = 0; i < log.p(); i++) {
    REQUIRE( log.coord_prox(w, i, 2) == prox(i) );
    REQUIRE( prox(i) == true_prox(i) );
  }

}

/*
________________________________________________________________________________

  LOGISTIC + L2 Reg
________________________________________________________________________________

 */

TEST_CASE( "LOGISTIC + L2 LOSS", "[loss]" ) {

  MatrixXd X {
    {1, 2, 3},
    {4, 5, 6}
  };
  VectorXd y {{ 1, -1 }};
  VectorXd w {{ 1, 2, 3 }};

  L2Regularizer reg(1.0);
  Logistic log(X, y, &reg);

  // functions evaluation
  REQUIRE( log.evaluate(w) == Approx(23.000000415764195) );

  // gradients
  VectorXd grad = log.gradient(w);
  VectorXd true_grad {{ 2.99999958, 4.49999917, 5.99999875 }};
  for(unsigned int i = 0; i < grad.size(); i++) {
    REQUIRE( log.coord_gradient(w, i) == grad(i) );
    REQUIRE( grad(i) == Approx(true_grad(i)) );
  }

  // problem dimensions
  REQUIRE( log.n() == 2 );
  REQUIRE( log.p() == 3 );

  // residuals
  VectorXd residuals = log.get_residuals(w);
  VectorXd true_residuals {{ 14, -32 }};

  for(unsigned int i = 0; i < residuals.size(); i++) {
    REQUIRE( log.get_pointwise_residual(w, i, residuals) == residuals(i) );
    REQUIRE( residuals(i) == Approx(true_residuals(i)) );
  }

  VectorXd w_updated {{ 2, 2, 3 }};
  REQUIRE( *log.coord_update_residuals(residuals, 1, 0) == log.get_residuals(w_updated) );


  // lipschitz constants
  VectorXd coord_lip = log.get_all_lipschitz_coord();
  VectorXd true_lip {{ 3.125, 4.625, 6.625 }};
  for(unsigned int i = 0; i < coord_lip.size(); i++) {
    REQUIRE( coord_lip(i) == log.coord_lipschitz(i) );
    REQUIRE( coord_lip(i) == Approx(true_lip(i)) );
  }
  REQUIRE( log.lipschitz() == Approx(12.30033406578173) );

  // proximal
  VectorXd prox = log.prox(w, 10);
  for(unsigned int i = 0; i < log.p(); i++) {
    REQUIRE( log.coord_prox(w, i, 10) == prox(i) );
    REQUIRE( prox(i) == w(i) );
  }

}
