#include <catch2/catch.hpp>

#include "../include/loss"
#include "../include/optimizer"
#include "../include/utils/data_utils.hpp"

void TEST_CONVERGENCE(Loss* loss,
		      const double& opt,
		      const double& lr = 1.0) {

  VectorXd w0 = VectorXd::Zero(loss->p());
  Log cd = coordinate_descent(loss, w0, 15000, lr);
  Log gd = gradient_descent(loss, w0, 100, lr);

  REQUIRE( cd.final_obj == Approx(opt) );
  REQUIRE( gd.final_obj == Approx(opt) );

}

TEST_CASE( "OPTIMIZERS", "[optimizer]" ) {
  vector<string> header;
  MatrixXd datareg = read_csv<MatrixXd>("../tests/datasets/regression_data.csv", header);
  MatrixXd Xreg = drop_column(datareg, header, "y");
  VectorXd yreg = get_target(datareg, header, "y");
  VectorXd w0 = VectorXd::Zero(Xreg.cols());

  MatrixXd dataclf = read_csv<MatrixXd>("../tests/datasets/classification_data.csv", header);
  MatrixXd Xclf = drop_column(dataclf, header, "y");
  VectorXd yclf = get_target(dataclf, header, "y");

  Regularizer* nreg = new NullRegularizer();
  Regularizer* l1reg = new L1Regularizer(0.05);
  Regularizer* l2reg = new L2Regularizer(0.01);

  Loss* ls = create_loss("ls", Xreg, yreg, nreg);
  Loss* lasso = create_loss("ls", Xreg, yreg, l1reg);
  Loss* ridge = create_loss("ls", Xreg, yreg, l2reg);

  Loss* log = create_loss("log", Xclf, yclf, nreg);
  Loss* logl1 = create_loss("log", Xclf, yclf, l1reg);
  Loss* logl2 = create_loss("log", Xclf, yclf, l2reg);

  // references values are computed using SKLearn
  // see optimals.py and dataset_generation.py for more details

  TEST_CONVERGENCE(/* loss : */ ls,
		   /* opt  : */ 4.242083907350237);
  TEST_CONVERGENCE(/* loss : */ lasso,
		   /* opt  : */ 27.970577816353497);
  TEST_CONVERGENCE(/* loss : */ ridge,
		   /* opt  : */ 170.8171414602839);
  TEST_CONVERGENCE(/* loss : */ log,
		   /* opt  : */ 0.5030606576584011);
  TEST_CONVERGENCE(/* loss : */ logl1,
		   /* opt  : */ 0.5924929668790998);
  TEST_CONVERGENCE(/* loss : */ logl2,
		   /* opt  : */ 0.5090745569374097);

  delete nreg, l1reg, l2reg;
  delete ls, lasso, ridge, log, logl1, logl2;

}
