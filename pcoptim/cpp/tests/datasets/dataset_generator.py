import os
import pandas as pd
from sklearn.datasets import make_classification, make_regression
from sklearn import linear_model

from optimals import optimal_objective

# parameters
n = 100
p = 10
seed = 42
data_dir = "."

# create directory if needed
if not os.path.exists(data_dir):
    os.makedirs(data_dir)

# generate regression dataset
Xreg, yreg = make_regression(n, p, noise=3, random_state=seed)
regression_data = pd.DataFrame(Xreg)
regression_data["y"] = yreg
regression_data.to_csv(data_dir + "/regression_data.csv", index=False)

# generate classification dataset
Xclf, yclf = make_classification(n, p, flip_y=0.4, random_state=seed)
yclf[yclf == 0] = -1
classification_data = pd.DataFrame(Xclf)
classification_data["y"] = yclf
classification_data.to_csv(data_dir + "/classification_data.csv", index=False)


# compute optimal regression values
print(optimal_objective(Xreg, yreg, "ls", "none", 0.0)[1])
print(optimal_objective(Xreg, yreg, "ls", "l1", 0.05)[1])
print(optimal_objective(Xreg, yreg, "ls", "l2", 0.01)[1])


print(optimal_objective(Xclf, yclf, "log", "none", 0.0)[1])
print(optimal_objective(Xclf, yclf, "log", "l1", 0.05)[1])
print(optimal_objective(Xclf, yclf, "log", "l2", 0.01)[1])
