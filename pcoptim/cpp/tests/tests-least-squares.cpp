#include <catch2/catch.hpp>
#include <Eigen/Dense>
#include <iostream>

#include "../include/loss"


using Eigen::MatrixXd;
using Eigen::VectorXd;


/*
________________________________________________________________________________

  LEAST SQUARES
________________________________________________________________________________

 */

TEST_CASE( "LEAST SQUARES LOSS", "[loss]" ) {

  MatrixXd X {
    {1, 2, 3},
    {4, 5, 6}
  };
  VectorXd y {{ 1, 2 }};
  VectorXd w {{ 1, 2, 3 }};

  LeastSquares ls(X, y);

  // functions evaluation
  REQUIRE( ls.evaluate(w) == Approx(267.25) );

  // gradients
  VectorXd grad = ls.gradient(w);
  VectorXd true_grad {{ 66.5, 88.0, 109.5 }};
  for(unsigned int i = 0; i < grad.size(); i++) {
    REQUIRE( ls.coord_gradient(w, i) == grad(i) );
    REQUIRE( grad(i) == Approx(true_grad(i)) );
  }

  // problem dimensions
  REQUIRE( ls.n() == 2 );
  REQUIRE( ls.p() == 3 );

  // residuals
  VectorXd residuals = ls.get_residuals(w);
  VectorXd true_residuals {{ 13, 30 }};

  for(unsigned int i = 0; i < residuals.size(); i++) {
    REQUIRE( ls.get_pointwise_residual(w, i, residuals) == residuals(i) );
    REQUIRE( residuals(i) == Approx(true_residuals(i)) );
  }

  VectorXd w_updated {{ 2, 2, 3 }};
  REQUIRE( *ls.coord_update_residuals(residuals, 1, 0) == ls.get_residuals(w_updated) );


  // lipschitz constants
  VectorXd coord_lip = ls.get_all_lipschitz_coord();
  VectorXd true_lip {{ 8.5, 14.5, 22.5 }};
  for(unsigned int i = 0; i < coord_lip.size(); i++) {
    REQUIRE( coord_lip(i) == ls.coord_lipschitz(i) );
    REQUIRE( coord_lip(i) == Approx(true_lip(i)) );
  }
  REQUIRE( ls.lipschitz() == Approx(45.2013362631) );

  // proximal
  VectorXd prox = ls.prox(w, 10);
  for(unsigned int i = 0; i < ls.p(); i++) {
    REQUIRE( ls.coord_prox(w, i, 10) == prox(i) );
    REQUIRE( prox(i) == w(i) );
  }

}


/*
________________________________________________________________________________

  LASSO
________________________________________________________________________________

 */

TEST_CASE( "LASSO LOSS", "[loss]" ) {

  MatrixXd X {
    {1, 2, 3},
    {4, 5, 6}
  };
  VectorXd y {{ 1, 2 }};
  VectorXd w {{ 1, 2, 3 }};

  L1Regularizer reg(1.0);
  LeastSquares lasso(X, y, &reg);

  // functions evaluation
  REQUIRE( lasso.evaluate(w) == Approx(273.25) );

  // gradients
  VectorXd grad = lasso.gradient(w);
  VectorXd true_grad {{ 66.5, 88.0, 109.5 }};
  for(unsigned int i = 0; i < grad.size(); i++) {
    REQUIRE( lasso.coord_gradient(w, i) == grad(i) );
    REQUIRE( grad(i) == Approx(true_grad(i)) );
  }

  // problem dimensions
  REQUIRE( lasso.n() == 2 );
  REQUIRE( lasso.p() == 3 );

  // residuals
  VectorXd residuals = lasso.get_residuals(w);
  VectorXd true_residuals {{ 13, 30 }};

  for(unsigned int i = 0; i < residuals.size(); i++) {
    REQUIRE( lasso.get_pointwise_residual(w, i, residuals) == residuals(i) );
    REQUIRE( residuals(i) == Approx(true_residuals(i)) );
  }

  VectorXd w_updated {{ 2, 2, 3 }};
  REQUIRE( *lasso.coord_update_residuals(residuals, 1, 0) == lasso.get_residuals(w_updated) );


  // lipschitz constants
  VectorXd coord_lip = lasso.get_all_lipschitz_coord();
  VectorXd true_lip {{ 8.5, 14.5, 22.5 }};
  for(unsigned int i = 0; i < coord_lip.size(); i++) {
    REQUIRE( coord_lip(i) == lasso.coord_lipschitz(i) );
    REQUIRE( coord_lip(i) == Approx(true_lip(i)) );
  }
  REQUIRE( lasso.lipschitz() == Approx(45.2013362631) );

  // proximal
  VectorXd prox = lasso.prox(w, 2);
  VectorXd true_prox {{ 0, 0, 1 }};
  for(unsigned int i = 0; i < lasso.p(); i++) {
    REQUIRE( lasso.coord_prox(w, i, 2) == prox(i) );
    REQUIRE( prox(i) == true_prox(i) );
  }

}

/*
________________________________________________________________________________

  RIDGE
________________________________________________________________________________

 */

TEST_CASE( "RIDGE LOSS", "[loss]" ) {

  MatrixXd X {
    {1, 2, 3},
    {4, 5, 6}
  };
  VectorXd y {{ 1, 2 }};
  VectorXd w {{ 1, 2, 3 }};

  L2Regularizer reg(1.0);
  LeastSquares ridge(X, y, &reg);

  // functions evaluation
  REQUIRE( ridge.evaluate(w) == Approx(274.25) );

  // gradients
  VectorXd grad = ridge.gradient(w);
  VectorXd true_grad {{ 67.5, 90.0, 112.5 }};
  for(unsigned int i = 0; i < grad.size(); i++) {
    REQUIRE( ridge.coord_gradient(w, i) == grad(i) );
    REQUIRE( grad(i) == Approx(true_grad(i)) );
  }

  // problem dimensions
  REQUIRE( ridge.n() == 2 );
  REQUIRE( ridge.p() == 3 );

  // residuals
  VectorXd residuals = ridge.get_residuals(w);
  VectorXd true_residuals {{ 13, 30 }};

  for(unsigned int i = 0; i < residuals.size(); i++) {
    REQUIRE( ridge.get_pointwise_residual(w, i, residuals) == residuals(i) );
    REQUIRE( residuals(i) == Approx(true_residuals(i)) );
  }

  VectorXd w_updated {{ 2, 2, 3 }};
  REQUIRE( *ridge.coord_update_residuals(residuals, 1, 0) == ridge.get_residuals(w_updated) );


  // lipschitz constants
  VectorXd coord_lip = ridge.get_all_lipschitz_coord();
  VectorXd true_lip {{ 9.5, 15.5, 23.5 }};
  for(unsigned int i = 0; i < coord_lip.size(); i++) {
    REQUIRE( coord_lip(i) == ridge.coord_lipschitz(i) );
    REQUIRE( coord_lip(i) == Approx(true_lip(i)) );
  }
  REQUIRE( ridge.lipschitz() == Approx(46.2013362631) );

  // proximal
  VectorXd prox = ridge.prox(w, 2);
  for(unsigned int i = 0; i < ridge.p(); i++) {
    REQUIRE( ridge.coord_prox(w, i, 2) == prox(i) );
    REQUIRE( prox(i) == w(i) );
  }

}
