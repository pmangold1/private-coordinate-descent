#include <catch2/catch.hpp>

#include "../include/loss"


using Eigen::MatrixXd;
using Eigen::VectorXd;


void TEST_REGULARIZER(Regularizer* reg,
		      const VectorXd& input,
		      const VectorXd& input_min_abs_subgradient,
		      const double& param,
		      const double& true_objective,
		      const VectorXd& true_gradient,
		      const VectorXd& true_prox,
		      const double& true_lipschitz,
		      const VectorXd& true_coord_lipschitz,
		      const VectorXd& true_min_abs_subgradient) {

  // compute gradient at input
  VectorXd grad = reg->gradient(input);

  // objective value
  REQUIRE ( reg->evaluate(input) == Approx(true_objective) );

  // gradients
  for(unsigned int i = 0; i < input.size(); i++) {
    REQUIRE( reg->coord_gradient(input, i) == Approx(true_gradient(i)) );
    REQUIRE( grad(i) == Approx(true_gradient(i)) );
  }

  // lipschitz constants
  REQUIRE( reg->lipschitz() == Approx(true_lipschitz) );
  for(unsigned int i = 0; i < input.size(); i++) {
    REQUIRE( reg->coord_lipschitz(i) == Approx(true_coord_lipschitz(i)) );
  }

  // gradients
  for(unsigned int i = 0; i < input.size(); i++) {
    REQUIRE( reg->coord_prox(input, i, 2) == Approx(true_prox(i)) );
    REQUIRE( reg->prox(input, 2)(i) == Approx(true_prox(i)) );
  }

  // minimum absolute subgradient
  for(unsigned int j = 0; j < input.size(); j++) {
    REQUIRE( reg->min_abs_subgradient(input_min_abs_subgradient, input)(j)
	     == Approx(true_min_abs_subgradient(j)) );
    REQUIRE( reg->coord_min_abs_subgradient(input_min_abs_subgradient, input, j)
	     == Approx(true_min_abs_subgradient(j)) );
  }

}

//____________________________________________________________________________//


TEST_CASE( "NULL REGULARIZER", "[loss][regularizer]" ) {
  NullRegularizer nreg;
  NullRegularizer nreg_param(1.0);

  VectorXd input(10); input << 1, 2, 3, 4, 5, -1, -2, -3, -4, -5;

  TEST_REGULARIZER(/* regularizer               : */ &nreg,
		   /* input                     : */ input,
		   /* input_min_abs_subgradient : */ VectorXd::Zero(10),
		   /* param                     : */ 0.0,
		   /* true_objective            : */ 0,
		   /* true_gradient             : */ VectorXd::Zero(10),
		   /* true_prox                 : */ input,
		   /* true_lipschitz            : */ 0,
		   /* true_coord_lipschitz      : */ VectorXd::Zero(10),
		   /* true_min_abs_subgradient  : */ input.array().abs());


    TEST_REGULARIZER(/* regularizer               : */ &nreg_param,
		     /* input                     : */ input,
		     /* input_min_abs_subgradient : */ VectorXd::Zero(10),
		     /* param                     : */ 0.0,
		     /* true_objective            : */ 0,
		     /* true_gradient             : */ VectorXd::Zero(10),
		     /* true_prox                 : */ input,
		     /* true_lipschitz            : */ 0,
		     /* true_coord_lipschitz      : */ VectorXd::Zero(10),
		     /* true_min_abs_subgradient  : */ input.array().abs());

}


//____________________________________________________________________________//

TEST_CASE( "L2 REGULARIZER", "[loss][regularizer]" ) {
  double param = 152.5;
  L2Regularizer l2reg(param);

  VectorXd input(10); input << 1, 2, 3, 4, 5, -1, -2, -3, -4, -5;

  TEST_REGULARIZER(/* regularizer               : */ &l2reg,
		   /* input                     : */ input,
		   /* input_min_abs_subgradient : */ VectorXd::Zero(10),
		   /* param                     : */ param,
		   /* true_objective            : */ param * 55,
		   /* true_gradient             : */ param * input,
		   /* true_prox                 : */ input,
		   /* true_lipschitz            : */ param,
		   /* true_coord_lipschitz      : */ VectorXd::Constant(10, param),
		   /* true_min_abs_subgradient  : */ input.array().abs());
}

//____________________________________________________________________________//


TEST_CASE( "L1 REGULARIZER", "[loss][regularizer]" ) {
  double param = 1.5;
  L1Regularizer l1reg(param);

  VectorXd input(10); input << 1, 2, 3, 4, 5, -1, -2, -3, -4, -5;
  VectorXd true_prox(10); true_prox << 0, 0, 0, 1, 2, 0, 0, 0, -1, -2;;
  VectorXd true_min_abs_subgradient_zero(10); true_min_abs_subgradient_zero << 0, 0.5, 1.5, 2.5, 3.5, 0, 0.5, 1.5, 2.5, 3.5;
  VectorXd true_min_abs_subgradient_nonzero(10); true_min_abs_subgradient_nonzero << 2.5, 3.5, 4.5, 5.5, 6.5, 2.5, 3.5, 4.5, 5.5, 6.5;

  TEST_REGULARIZER(/* regularizer               : */ &l1reg,
		   /* input                     : */ input,
		   /* input_min_abs_subgradient : */ VectorXd::Zero(10),
		   /* param                     : */ param,
		   /* true_objective            : */ param * 30,
		   /* true_gradient             : */ VectorXd::Zero(10),
		   /* true_prox                 : */ true_prox,
		   /* true_lipschitz            : */ 0,
		   /* true_coord_lipschitz      : */ VectorXd::Zero(10),
		   /* true_min_abs_subgradient  : */ true_min_abs_subgradient_zero);


  TEST_REGULARIZER(/* regularizer               : */ &l1reg,
		   /* input                     : */ input,
		   /* input_min_abs_subgradient : */ VectorXd::Constant(10, 10),
		   /* param                     : */ param,
		   /* true_objective            : */ param * 30,
		   /* true_gradient             : */ VectorXd::Zero(10),
		   /* true_prox                 : */ true_prox,
		   /* true_lipschitz            : */ 0,
		   /* true_coord_lipschitz      : */ VectorXd::Zero(10),
		   /* true_min_abs_subgradient  : */ true_min_abs_subgradient_nonzero);
}
